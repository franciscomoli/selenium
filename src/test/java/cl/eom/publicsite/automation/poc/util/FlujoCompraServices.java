package cl.eom.publicsite.automation.poc.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mifmif.common.regex.Generex;

import cl.eom.publicsite.automation.constants.Constants;
import cl.eom.publicsite.automation.poc.business.flow.BaseFlow;
import cl.eom.publicsite.automation.poc.definition.steps.FlujoNegocio_Definition;
import cl.eom.publicsite.automation.poc.model.Producto;
import cl.eom.publicsite.automation.poc.model.CompraSingleton;;

public final class FlujoCompraServices {
	private static final Log log = LogFactory.getLog(FlujoCompraServices.class);
	
	
//	public static void createSheetForNameTestCase(String nombreHoja,String nombreCasoPrueba,List<String> datos) throws Exception {
//		String[] tc = nombreCasoPrueba.split("\\-");
//		
//		if(nombreHoja== null) {
//			flujoNegocio.nombreHoja = tc[0];
//			ManagementMicrosoftService.createNewSheet(FlujoCompraCompletoDefinition_UsuarioRegistrado.nombreHoja);
//			ManagementMicrosoftService.WriteSheetExcel(FlujoCompraCompletoDefinition_UsuarioRegistrado.nombreHoja,datos);
//		}else {
//			if(!nombreHoja.equals(tc[0])) {
//				FlujoCompraCompletoDefinition_UsuarioRegistrado.nombreHoja = tc[0];
//				ManagementMicrosoftService.createNewSheet(FlujoCompraCompletoDefinition_UsuarioRegistrado.nombreHoja);
//				ManagementMicrosoftService.WriteSheetExcel(FlujoCompraCompletoDefinition_UsuarioRegistrado.nombreHoja,datos);
//			}else {
//				ManagementMicrosoftService.WriteSheetExcel(FlujoCompraCompletoDefinition_UsuarioRegistrado.nombreHoja,datos);
//			}
//		}
//	}

	public static void createSheetForNameTestCaseOrder(List<String> datos)
			throws Exception {
//		String[] tc = nombreCasoPrueba.split("\\-");
//		if (nombreHoja == null) {
//			FlujoCompraCompleto_NuevoRegistroDefinition.nombreHoja = tc[0];
//			ManagementMicrosoftService.createNewSheet(nombreHoja);
		
		//PARA CREAR LA HOJA DE EXCEL
//			ManagementMicrosoftService.WriteSheetExcel(FlujoCompraCompleto_NuevoRegistroDefinition.nombreHoja, datos);
//		} else {
//			if (!nombreHoja.equals(tc[0])) {
//				FlujoCompraCompleto_NuevoRegistroDefinition.nombreHoja = tc[0];
//				ManagementMicrosoftService.createNewSheet(FlujoCompraCompleto_NuevoRegistroDefinition.nombreHoja);
//				ManagementMicrosoftService.WriteSheetExcel(FlujoCompraCompleto_NuevoRegistroDefinition.nombreHoja,
//						datos);
//			} else {
//				ManagementMicrosoftService.WriteSheetExcel(FlujoCompraCompleto_NuevoRegistroDefinition.nombreHoja,
//						datos);
//			}
//		}
	}
	public static List<String> createDataForExcel(String tc,String login,String usuario,String sku, String despacho, String despachoDomicilio, String metodoPago, String numeroTarjeta,String tipoProducto,String factura) {
//		Map<String, String> MapDatos = new LinkedHashMap<String, String>();
//		MapDatos.put("numOC", "");
		List<String> datos = new ArrayList<String>();
		datos.add("OC No generada");
		datos.add(usuario);
		String tipoOrden = factura.equals("true")?tipoProducto+" With Factura":tipoProducto+" No Factura"; 
		datos.add( tipoOrden);
		datos.add(sku);
		String tipoDelivery = despacho.equals("domicilio")?"Home Delivery":"Collect from store";
		datos.add(tipoDelivery);
		String subtypeDelivery="";
		if(despacho.equals("domicilio")) {
			subtypeDelivery = despachoDomicilio.equalsIgnoreCase("estandar")?"Standard":"Programmed";
		}
		datos.add(subtypeDelivery);
		String[] paymentMethod = metodoPago.split("C"); 
		datos.add(paymentMethod[0]+" "+"Card" );
		datos.add(numeroTarjeta);
		return datos;
	}
	public static List<String> createDataForExcel(String tc, String sku,String rut, String despacho, String despachoDomicilio,String metodoPago, String numeroTarjeta, String tipoProducto, String factura) {
		// Map<String, String> MapDatos = new LinkedHashMap<String, String>();
		// MapDatos.put("numOC", "");
		List<String> datos = new ArrayList<String>();
		datos.add("OC No Generada");
		datos.add("New Registered");
		datos.add("Nombre");
		datos.add(rut);
		String tipoOrden = factura.equals("true") ? tipoProducto + " With Factura" : tipoProducto + " No Factura";
		datos.add(tipoOrden);
		datos.add(sku);
		String tipoDelivery = despacho.equals("domicilio") ? "Home Delivery" : "Collect from store";
		datos.add(tipoDelivery);
		String subtypeDelivery = despachoDomicilio.equalsIgnoreCase("standard") ? "Standard" : "Programmed";
		datos.add(subtypeDelivery);
		datos.add("Fecha Entrega");
		String[] paymentMethod = metodoPago.split("C");
		datos.add(paymentMethod[0] + " " + "Card");
		datos.add(numeroTarjeta);
		return datos;
	}
	
	public static Boolean isValidLogin() throws Exception { 
		boolean valid = false; 
		if( BaseFlow.driver.findElements(By.className("error-form")).size() == 0) {
			valid = true;
		} else {
			valid = false;
		}  
		return valid;
	}
	
	public static Boolean isValidCdgoDescuento() throws Exception { 
		boolean valid = false; 
		if( BaseFlow.driver.findElements(By.className("error")).size() == 0) {
			valid = true;
		} else {
			valid = false;
		}  
		return valid;
	}
	
//	public static void ingresarSkuAndSearch(String sku){
//		try {
//
//			if (Constants.Browser.contains("Mobile")) {
//				
//				GenericMethods.implicityWait(10, By.xpath("(//span[@class='icon-search'])[3]"));
//				WebElement btnBuscarMobile = BaseFlow.driver.findElement(By.xpath("(//span[@class='icon-search'])[3]"));
//				JavascriptExecutor executor = (JavascriptExecutor) BaseFlow.driver;
//				executor.executeScript("arguments[0].click();", btnBuscarMobile);
//			}
//			WebElement search = GenericMethods.implicityWait(10, By.id("q"));
//			search.clear(); 
//			BaseFlow.pageModel.getHomeTxtIngresobusqueda().sendKeys(sku);
////			Thread.sleep(2000);
//			JavascriptExecutor executor = (JavascriptExecutor) BaseFlow.driver;
//			executor.executeScript("arguments[0].click();", BaseFlow.pageModel.getHomeBtnBuscar());
//
//		} catch (Exception e) {
//			log.info("",e);
//		}
//	}
	
//	public static void seleccionarCantidad(String cantidad) {
//		try {
////			GenericMethods.waitTime(BaseFlow.pageModel.getPdpCantidad());
//			GenericMethods.waitForClickeable(20,By.id("Quantity"));
//			GenericMethods.scrollElement(BaseFlow.driver,BaseFlow.pageModel.getPdpCantidad());
//			Select pdpCantidad = new Select(BaseFlow.pageModel.getPdpCantidad());
//			pdpCantidad.selectByVisibleText(cantidad);
//		} catch (Exception e) {
//			log.error(e);
//		}
//	}
	
//	public static void seleccionarColor(String tipoProducto) throws Exception {
//		if(!tipoProducto.equalsIgnoreCase(Constants.productoOption) && !tipoProducto.equalsIgnoreCase(Constants.productoStandard)){
//			if(BaseFlow.pageModel.getCheckOutRadioColores().size() != 0 && BaseFlow.driver.findElements(By.xpath("//ul[@class='swatches color']")).size() > 0) {
//				if(BaseFlow.pageModel.getCheckOutRadioColores().get(0).isSelected()){
//					BaseFlow.pageModel.getCheckOutRadioColores().get(1).click();
//				}else if (BaseFlow.driver.findElements(By.xpath("//ul[@class=\"swatches color\"]")).size() > 0) {
//					BaseFlow.pageModel.getCheckOutRadioColores().get(0).click();
//					Thread.sleep(1000);
//				}
//			}	
//		}
//	}
//	
//	public static void seleccionarCantidadEnCarroCompra(String cantidadEnCarro) {
//		try {
//			if(!cantidadEnCarro.equals("")) {
//				 if(BaseFlow.pageModel.getCarroListCantidadProductoEnCarroCompras().size()==0) {
//					 Select pdpCantidad = new Select(BaseFlow.pageModel.getCarroListCantidadProductoEnCarroCompras().get(0));
//					pdpCantidad.selectByVisibleText(cantidadEnCarro);
//				 }else {
//					 Select pdpCantidad = new Select(BaseFlow.pageModel.getCarroListCantidadProductoEnCarroCompras().get(1));
//					 pdpCantidad.selectByVisibleText(cantidadEnCarro);
//				 }
//			 }
//		} catch (Exception e) {
//		}
//	}
	
//	public static void seleccionarTalla(String tipoProducto) {
//		try {
//			String talla = "";
//			if(!tipoProducto.equalsIgnoreCase(Constants.productoOption) && !tipoProducto.equalsIgnoreCase(Constants.productoStandard)){
//				if(GenericMethods.existElement(By.id("va-size"))) {
//					GenericMethods.waitTime(BaseFlow.pageModel.getCheckOutSelectTalla());
//					Select pdpSelectTalla = new Select(BaseFlow.pageModel.getCheckOutSelectTalla());
//					if(pdpSelectTalla.getOptions().size()!=0) {
//						for(WebElement option:pdpSelectTalla.getOptions()) {
//							if(!option.getText().equalsIgnoreCase("Seleccionar Talla")) {
//								if(option.isEnabled()) {
//									talla = option.getText();
//									break;
//								}
//							}
//						}
//						pdpSelectTalla.selectByVisibleText(talla);
//					}
//				}
//			}
//		} catch (Exception e) {
//			log.error(e);
//		}
//	}
	
	
	
	
	public static boolean datoPruebaIsValid(String valorPrueba) {
		boolean valid = false;
		if(valorPrueba != null && !valorPrueba.equals("")) {
			valid = true;
		}
		return valid;
	}
	
	
	
	public static String generaMail() {
		Generex generex = new Generex("(\\w10)([a-z0-9]+)[@]([a-z0-9]+)[.]\\com");
		String randomStr = generex.random();
		return randomStr;
	}
	
	public static String generarRut() {
		String rut = String.valueOf(new Random().nextInt(9999999) + 10000000);
		int multi = 2;
		int acum = 0;
		for (int i = rut.length() - 1; i > -1; i--) {
			if (multi > 7) {
				multi = 2;
			}
			acum += multi * Integer.parseInt(rut.substring(i, i + 1));
			multi++;
		}
		int dv = 11 - (acum % 11);
		String dvf = "";
		if (dv > 9) {
			if (dv == 10) {
				dvf = "K";
			}
			if (dv == 11) {
				dvf = "0";
			}
		} else {
			dvf = String.valueOf(dv);
		}
		return rut + "-" + dvf;
	}
	
	public static Integer convert(String num) throws Exception {
//		String cadena = (num.replaceAll("\\D+", ""));
//		String[] splitCadena = cadena.split(" ");
//		String part1 = splitCadena[0];
//		return Integer.parseInt(part1);
		//TODO REVISAR FUNCION PARA MOBILE 
		return Integer.parseInt(num.replaceAll("\\D+", ""));
		

	}}
	
	

	
//	public static void registroNuevoUsuario(String nombre, String apellido, String mail, String rut,String codTel, String telefono, String tipoDocumento, String nDocumento, String clave) throws Exception {
//		
//		// GenericMethods.waitTime(BaseFlow.pageModelOrder.getHomeLblLogin());
//		Thread.sleep(2000);
//		GenericMethods.implicityWait(15, By.id("clickLogin"));
//		BaseFlow.pageModelOrder.getHomeLblLogin().click();
//		GenericMethods.captureBitmap();
//		// INGRESO INFORMACION DE REGISTRO
//		BaseFlow.pageModelOrder.getLoginTxtNombre().sendKeys(nombre);
//		BaseFlow.pageModelOrder.getLoginTxtApellido().sendKeys(apellido);
//		
//  //mail seteado por f(x) que genera mail
//		BaseFlow.pageModelOrder.getLoginTxtEmail().sendKeys(mail);
//
//		BaseFlow.pageModelOrder.getLoginComboCodTel().sendKeys(codTel);
//		BaseFlow.pageModelOrder.getLoginTxtTelefono().sendKeys(telefono);
//		GenericMethods.captureBitmap();
//
//		if (tipoDocumento.equals("rut")) {
//			BaseFlow.pageModelOrder.getLoginRadioBtnRut().click();
//			//rut seteado por funcion que crea rut
//			BaseFlow.pageModelOrder.getLoginTxtRut().sendKeys(rut);
//			GenericMethods.captureBitmap();
//		} else if (tipoDocumento.equals("dni")) {
//			BaseFlow.pageModelOrder.getLoginRadioBtnDni().click();
//			BaseFlow.pageModelOrder.getLoginTxtDni().sendKeys(nDocumento);
//			GenericMethods.captureBitmap();
//		}
//		BaseFlow.pageModelOrder.getLoginTxtClaveReg().sendKeys(clave);
//		BaseFlow.pageModelOrder.getLoginTxtClaveRegconfirm().sendKeys(clave);
//		BaseFlow.pageModelOrder.getLoginTxtClaveRegconfirm().sendKeys(Keys.TAB);
//		GenericMethods.captureBitmap();
//		GenericMethods.waitTime(BaseFlow.pageModelOrder.getLoginBtnRegistrar());
//		GenericMethods.scrollClickElement(BaseFlow.driver, BaseFlow.pageModelOrder.getLoginBtnRegistrar());
//	}


