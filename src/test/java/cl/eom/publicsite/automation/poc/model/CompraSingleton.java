package cl.eom.publicsite.automation.poc.model;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

public class CompraSingleton {
	
	public List<Producto> producto;
	public String sku;
	public String tipoProducto;
	public String cantidad;
	public String armado;
	public String garantia;
	public String nombre;
	public String color;
	public String talla;
	public String precioDefecto;
	public String precioNormal;
	public String precioInternet;
	public String precioPromocional;
	public String precioTarjetaCencosud;
	public String tipoTalla;
	public String stockProductoRecomendado;
	public String skuAgregadoFavorito;
	public String precioDefectoRecomendado;
	public String precioNormalRecomendado;
	public String precioInternetRecomendado;
	public String precioPromocionalRecomendado;
	public String precioTarjetaCencosudRecomendado;
	public String nombreRecomendado;
	public String skuPreciosJSON = "";
	List<WebElement> mensajePreSale = new ArrayList<>();
	List<String> mensajePromotion = new ArrayList<>();
	public String email;
	public String telefono;
	
	
	

	
	
	private static CompraSingleton instance = null;
	
	
	
	
	
	

	public String getSkuPreciosJSON() {
		return skuPreciosJSON;
	}

	public void setSkuPreciosJSON(String skuPreciosJSON) {
		this.skuPreciosJSON = skuPreciosJSON;
	}

	public String getPrecioDefectoRecomendado() {
		return precioDefectoRecomendado;
	}

	public void setPrecioDefectoRecomendado(String precioDefectoRecomendado) {
		this.precioDefectoRecomendado = precioDefectoRecomendado;
	}

	public String getPrecioNormalRecomendado() {
		return precioNormalRecomendado;
	}

	public void setPrecioNormalRecomendado(String precioNormalRecomendado) {
		this.precioNormalRecomendado = precioNormalRecomendado;
	}

	public String getPrecioInternetRecomendado() {
		return precioInternetRecomendado;
	}

	public void setPrecioInternetRecomendado(String precioInternetRecomendado) {
		this.precioInternetRecomendado = precioInternetRecomendado;
	}

	public String getPrecioPromocionalRecomendado() {
		return precioPromocionalRecomendado;
	}

	public void setPrecioPromocionalRecomendado(String precioPromocionalRecomendado) {
		this.precioPromocionalRecomendado = precioPromocionalRecomendado;
	}

	public String getPrecioTarjetaCencosudRecomendado() {
		return precioTarjetaCencosudRecomendado;
	}

	public void setPrecioTarjetaCencosudRecomendado(String precioTarjetaCencosudRecomendado) {
		this.precioTarjetaCencosudRecomendado = precioTarjetaCencosudRecomendado;
	}

	public String getNombreRecomendado() {
		return nombreRecomendado;
	}

	public void setNombreRecomendado(String nombreRecomendado) {
		this.nombreRecomendado = nombreRecomendado;
	}

	public List<WebElement> getMensajePreSale() {
		return mensajePreSale;
	}

	public void setMensajePreSale(List<WebElement> mensajePreSale) {
		this.mensajePreSale = mensajePreSale;
	}

	public List<String> getMensajePromotion() {
		return mensajePromotion;
	}

	public void setMensajePromotion(List<String> mensajePromotion) {
		this.mensajePromotion = mensajePromotion;
	}

	public String getSkuAgregadoFavorito() {
		return skuAgregadoFavorito;
	}

	public void setSkuAgregadoFavorito(String skuAgregadoFavorito) {
		this.skuAgregadoFavorito = skuAgregadoFavorito;
	}

	public String getStockProductoRecomendado() {
		return stockProductoRecomendado;
	}

	public void setStockProductoRecomendado(String stockProductoRecomendado) {
		this.stockProductoRecomendado = stockProductoRecomendado;
	}

	public String getTipoTalla() {
		return tipoTalla;
	}

	public void setTipoTalla(String tipoTalla) {
		this.tipoTalla = tipoTalla;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getTalla() {
		return talla;
	}

	public void setTalla(String talla) {
		this.talla = talla;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public String getCantidad() {
		return cantidad;
	}

	
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getArmado() {
		return armado;
	}

	public void setArmado(String armado) {
		this.armado = armado;
	}

	public String getGarantia() {
		return garantia;
	}

	public void setGarantia(String garantia) {
		this.garantia = garantia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrecioNormal() {
		return precioNormal;
	}

	public void setPrecioNormal(String precioNormal) {
		this.precioNormal = precioNormal;
	}

	public String getPrecioInternet() {
		return precioInternet;
	}

	public void setPrecioInternet(String precioInternet) {
		this.precioInternet = precioInternet;
	}

	public String getPrecioPromocional() {
		return precioPromocional;
	}

	public void setPrecioPromocional(String precioPromocional) {
		this.precioPromocional = precioPromocional;
	}

	public String getPrecioTarjetaCencosud() {
		return precioTarjetaCencosud;
	}

	public void setPrecioTarjetaCencosud(String precioTarjetaCencosud) {
		this.precioTarjetaCencosud = precioTarjetaCencosud;
	}

	public String getPrecioDefecto() {
		return precioDefecto;
	}

	public void setPrecioDefecto(String precioDefecto) {
		this.precioDefecto = precioDefecto;
	}
	

	public List<Producto> getProducto() {
		return producto;
	}

	public void setProducto(List<Producto> producto) {
		this.producto = producto;
	}

	public static void setInstance(CompraSingleton instance) {
		CompraSingleton.instance = instance;
	}

	public CompraSingleton() {
		
	}

	public static CompraSingleton getInstance() {
	      if(instance == null) {
	         instance = new CompraSingleton();
	      }
	      return instance;
	   }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	

}
