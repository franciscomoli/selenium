package cl.eom.publicsite.automation.poc.business.flow;

import org.junit.BeforeClass;
import org.junit.runners.Suite.SuiteClasses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cl.eom.publicsite.automation.poc.stage.set.flujoNegocio_StageSet;

@SuiteClasses({flujoNegocio_StageSet.class})

public class FlujoNegocio_RunTest extends BaseFlow{
    private static final Logger LOGGER = LoggerFactory.getLogger(flujoNegocio_StageSet.class);
    @BeforeClass
    public static void flag() throws Exception {
        LOGGER.info("Ejecutando flujo Compra Completo Nuevo Registro");
        //CREACION DE EXCEL
//        ManagementMicrosoftService.createNewFileExcel(FlujoCompraCompleto_NuevoRegistroDefinition.nombreHoja);
    }
}
