package cl.eom.publicsite.automation.poc.model;

public class Producto{
	public String sku;
	public String tipoProducto;
	public String cantidad;
	public String armado;
	public String garantia;
	public String nombre;
	public String color;
	public String talla;
	public String precioDefecto;
	public String precioNormal;
	public String precioInternet;
	public String precioPromocional;
	public String precioTarjetaCencosud;
	public String totalPrecioDefecto;
	public String totalPrecioNormal;
	public String totalPrecioInternet;
	public String totalPrecioPromocional;
	public String totalPrecioTarjetaCencosud;
	public String precioGarantia;
	public String precioArmado;
	public String tipoTalla;
	public String descuentoPrecioPromo;
	
	
	
	
	public String getPrecioGarantia() {
		return precioGarantia;
	}
	public void setPrecioGarantia(String precioGarantia) {
		this.precioGarantia = precioGarantia;
	}
	public String getPrecioArmado() {
		return precioArmado;
	}
	public void setPrecioArmado(String precioArmado) {
		this.precioArmado = precioArmado;
	}
	public String getTotalPrecioDefecto() {
		return totalPrecioDefecto;
	}
	public void setTotalPrecioDefecto(String totalPrecioDefecto) {
		this.totalPrecioDefecto = totalPrecioDefecto;
	}
	public String getTotalPrecioNormal() {
		return totalPrecioNormal;
	}
	public void setTotalPrecioNormal(String totalPrecioNormal) {
		this.totalPrecioNormal = totalPrecioNormal;
	}
	public String getTotalPrecioInternet() {
		return totalPrecioInternet;
	}
	public void setTotalPrecioInternet(String totalPrecioInternet) {
		this.totalPrecioInternet = totalPrecioInternet;
	}
	public String getTotalPrecioPromocional() {
		return totalPrecioPromocional;
	}
	public void setTotalPrecioPromocional(String totalPrecioPromocional) {
		this.totalPrecioPromocional = totalPrecioPromocional;
	}
	public String getTotalPrecioTarjetaCencosud() {
		return totalPrecioTarjetaCencosud;
	}
	public void setTotalPrecioTarjetaCencosud(String totalPrecioTarjetaCencosud) {
		this.totalPrecioTarjetaCencosud = totalPrecioTarjetaCencosud;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getTipoProducto() {
		return tipoProducto;
	}
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getArmado() {
		return armado;
	}
	public void setArmado(String armado) {
		this.armado = armado;
	}
	public String getGarantia() {
		return garantia;
	}
	public void setGarantia(String garantia) {
		this.garantia = garantia;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getTalla() {
		return talla;
	}
	public void setTalla(String talla) {
		this.talla = talla;
	}
	public String getPrecioDefecto() {
		return precioDefecto;
	}
	public void setPrecioDefecto(String precioDefecto) {
		this.precioDefecto = precioDefecto;
	}
	public String getPrecioNormal() {
		return precioNormal;
	}
	public void setPrecioNormal(String precioNormal) {
		this.precioNormal = precioNormal;
	}
	public String getPrecioInternet() {
		return precioInternet;
	}
	public void setPrecioInternet(String precioInternet) {
		this.precioInternet = precioInternet;
	}
	public String getPrecioPromocional() {
		return precioPromocional;
	}
	public void setPrecioPromocional(String precioPromocional) {
		this.precioPromocional = precioPromocional;
	}
	public String getPrecioTarjetaCencosud() {
		return precioTarjetaCencosud;
	}
	public void setPrecioTarjetaCencosud(String precioTarjetaCencosud) {
		this.precioTarjetaCencosud = precioTarjetaCencosud;
	}
	public String getTipoTalla() {
		return tipoTalla;
	}
	public void setTipoTalla(String tipoTalla) {
		this.tipoTalla = tipoTalla;
	}
	public String getDescuentoPrecioPromo() {
		return descuentoPrecioPromo;
	}
	public void setDescuentoPrecioPromo(String descuentoPrecioPromo) {
		this.descuentoPrecioPromo = descuentoPrecioPromo;
	}
	
	
}
