package cl.eom.publicsite.automation.poc.model;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ProductoOrder {

	private String sku;
	private String nombreProducto;
	private String tipoProducto; // standard-variation-promotion-bundle-presales
	private String color;
	private String talla;
	private int precioUnit;
	private int precioCenco;
	private int precioInternet;
	private int precioDescuento;
	private int cantidad;
	private int precioArmado;
	private int precioGarantia;
	private int descuento; //descuento de PDP aplicado al armado o garantia
	private boolean garantia;
	private boolean armado;
//	private static Producto instance;

	// CONSTRUCTOR
//	public static Producto getInstance() {
//		if (instance == null) {
//			instance = new Producto();
//		}
//		return instance;
//	}

	public ProductoOrder() {
	}

	public ProductoOrder(String sku, String nombreProducto, String tipoProducto, String color, String talla, int precioUnit,
			int precioDescuento, int precioCenco, int precioInternet, int cantidad, boolean garantia, int precioGarantia,boolean armado,int precioArmado, int descuento) {
		this.sku = sku;
		this.nombreProducto = nombreProducto;
		this.tipoProducto = tipoProducto;
		this.color = color;
		this.talla = talla;
		this.precioUnit = precioUnit;
		this.precioDescuento = precioDescuento;
		this.precioCenco = precioCenco;
		this.precioInternet = precioInternet;
		this.cantidad = cantidad;
		this.garantia = garantia;
		this.precioGarantia = precioGarantia;
		this.armado = armado;
		this.precioArmado = precioArmado;
		this.descuento = descuento;

	}

	@Override

	public String toString() {

		return "Producto-> SKU: " + sku + " Tipo Producto: " + tipoProducto + " Nombre: " + nombreProducto + " Color: "
				+ color + " Talla: " + talla + "\n" + " Precio Unitario: " + precioUnit + "Precio con Descuento: "
				+ precioDescuento + " Precio Internet: " + precioInternet + " Precio Cenco: " + precioCenco
				+ " Cantidad: " + cantidad + " Garantia: " + garantia + " Precio Garantia: " + precioGarantia
				+ " Armado: " + armado + " Precio Armado: " + precioArmado + " Descuento % (Armado o Garantia): " + descuento+"\n";

	}

	public int getPrecioArmado() {
		return precioArmado;
	}

	public void setPrecioArmado(int precioArmado) {
		this.precioArmado = precioArmado;
	}

	public int getPrecioGarantia() {
		return precioGarantia;
	}

	public void setPrecioGarantia(int precioGarantia) {
		this.precioGarantia = precioGarantia;
	}

	public int getPrecioDescuento() {
		return precioDescuento;
	}

	public void setPrecioDescuento(int precioDescuento) {
		this.precioDescuento = precioDescuento;
	}

	public String getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public int getPrecioUnit() {
		return precioUnit;
	}

	public void setPrecioUnit(int precioUnit) {
		this.precioUnit = precioUnit;
	}

	public int getPrecioCenco() {
		return precioCenco;
	}

	public void setPrecioCenco(int precioCenco) {
		this.precioCenco = precioCenco;
	}

	public int getPrecioInternet() {
		return precioInternet;
	}

	public void setPrecioInternet(int precioInternet) {
		this.precioInternet = precioInternet;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getTalla() {
		return talla;
	}

	public void setTalla(String talla) {
		this.talla = talla;
	}

	public boolean isGarantia() {
		return garantia;
	}

	public void setGarantia(boolean garantia) {
		this.garantia = garantia;
	}

	public boolean isArmado() {
		return armado;
	}

	public void setArmado(boolean armado) {
		this.armado = armado;
	}

	public int getDescuento() {
		return descuento;
	}

	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}
	

}
