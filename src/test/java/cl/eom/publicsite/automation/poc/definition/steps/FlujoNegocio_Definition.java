//EL DEFINITION NOS PROVEERA LOS METODOS PARA PODER REALIZAR EL FLUJO DE NEGOCIO
// SE DEBEN GENERAR LOS PASOS PARA REALIZAR EL FLUJO, YA QUE VALIDACIONES O FUNCIONES TRANSVERSALES DEBEN SER DESARROLLADAS EN EL SERVICE
//


package cl.eom.publicsite.automation.poc.definition.steps;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.WebElement;

import cl.eom.publicsite.automation.constants.Constants;
import cl.eom.publicsite.automation.poc.business.flow.BaseFlow;
import cl.eom.publicsite.automation.poc.model.ProductoOrder;
import cl.eom.publicsite.automation.poc.model.SingletonOrder;
import cl.eom.publicsite.automation.poc.util.FlujoCompraServices;
//import cl.eom.publicsite.automation.poc.model.ProductoOrder;
//import cl.eom.publicsite.automation.poc.model.SingletonOrder;
//import cl.eom.publicsite.automation.poc.util.FlujoCompraServices;
import cl.eom.publicsite.automation.poc.util.GenericMethods;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FlujoNegocio_Definition {
	private static final Log log = LogFactory.getLog(FlujoNegocio_Definition.class);

	public static SingletonOrder Singleton;
	public static WebElement element = null;
	String mail = FlujoCompraServices.generaMail();
	String rut = FlujoCompraServices.generarRut();
	List<String> datos = new ArrayList<String>();
	public static String nombreHoja = "nombreHoja";
	public static ProductoOrder p;

	@After
	public  void tearDown(Scenario scenario) throws Exception {
		GenericMethods.screenShotForScenarioFailed(scenario);
	}

	
	@Given("^ingreso url\"(.*?)\"$")
	public void ingreso_url(String url) throws Throwable {
		//instanciamos el Singleton y la clase Producto
		Singleton = new SingletonOrder();
		p = new ProductoOrder();

		BaseFlow.driver.manage().deleteAllCookies();
		BaseFlow.driver.get(Constants.URL_GOOGLE);
//		BaseFlow.driver.get(url);
		
		//TODO CREACIÓN DE EXCEL CON DATOS DE PRUEBA 
//		ManagementMicrosoftService.createNewFileExcel();
//		List<String> datos = FlujoCompraServices.createDataForExcel(tc, sku,rut,tipoDespacho, tipoEntrega, metodoPago,nTarjeta, tipoProducto, ingresaFactura);
//		FlujoCompraServices.createSheetForNameTestCaseOrder(datos);

	}

	@When("^Ingreso busqueda\"(.*?)\"$")
	public void ingreso_busqueda(String stringBusqueda) throws Throwable {
		BaseFlow.pageHome.getHomeTxtBusqueda().sendKeys(stringBusqueda);
		BaseFlow.pageHome.getHomeBtnBuscarConGoogle().click();
	}

	@When("^valido busqueda\"(.*?)\"$")
	public void valido_busqueda(String tipoBusqueda) throws Throwable {
	    //se setea en el singleton el tipoBusqueda en el atributo Nombre de la Clase SingletonOrder
		Singleton.setNombre(tipoBusqueda);
	    System.out.println("El SingletonOrder Singleton.getNombre: "+Singleton.getNombre());
	}

	@Then("^verifico que se despliegue la busqueda$")
	public void verifico_que_se_despliegue_la_busqueda() throws Throwable {
	    //seteamos un tipo de dato de la clase Producto
		p.setNombreProducto("Lavadora");
		System.out.println("El nombre de producto seteado en la Clase ProductoOrder p.getNombreProducto:" +p.getNombreProducto());
	    
	}

	@Then("^Valido despliegue de busqueda$")
	public void valido_despliegue_de_busqueda() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^Valido que se despliege de resultados de busqueda$")
	public void valido_que_se_despliege_de_resultados_de_busqueda() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}


//	@When("^Ingreso al registro\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"$")
//	public void ingreso_al_registro(String nombre, String apellido, String codTel, String telefono,String tipoDocumento, String nDocumento, String usuario, String clave, String tipoRegistro)
//			throws Throwable {
//		try {
//
//			FlujoCompraServices.ingresarDatosNuevoRegistro(nombre, apellido, mail, codTel, telefono, tipoDocumento, rut,clave, tipoRegistro);
//				
//			Singleton.setNombre(nombre + " " + apellido);
//			Singleton.setEmail(mail);
//			Singleton.setTelefono(codTel + telefono);
//
//		} catch (Exception e) {
//			log.error(e);
//			fail("error al hacer click en iniciar sesion ya que no se despliega contenedor de inico sesion ");
//		}
//
//				}
//
//	// SE DEBE ESPERAR A LA ENTREGA DEL MENSAJE DE ERROR EN CASO DE QUE EL USUARIO
//	// YA ESTE REGISTRADO
//	@When("^valido login ok_registro\"(.*?)\"$")
//	public void valido_login_ok_registro(String tipoRegistro) throws Throwable {
//		try {
//			if (!FlujoCompraServices.isValidLogin()) {
//				throw new PSCException("Login Erroneo = " + BaseFlow.pageModelOrder.getLoginLblError().getText());
//				}
//			} catch (PSCException e) {
//				log.info(e);
//				System.exit(0);
//			} catch (Exception e) {
//				log.info(" error en login : " + e);
//			}
//	}
//
//	@When("^ingreso sku_registro\"(.*?)\"$")
//	public void ingreso_sku_registro(String sku) throws Throwable {
//
//		try {
//			p = new ProductoOrder();
//			// MOUSEOVER AL SEARCH
//			// Actions action = new Actions(BaseFlow.driver);
//			// action.moveToElement(BaseFlow.pageModelOrder.getHomeBtnBuscar()).build().perform();
//			GenericMethods.scrollElement(BaseFlow.driver, BaseFlow.driver.findElement(By.id("q")));
//			FlujoCompraServices.ingresarSkuAndSearch(sku);
//			// SKU PRODUCTO
//			p.setSku(sku);
//
//			if (GenericMethods.existElement(By.xpath("//p[@class='not-found-search']"))) {
//				if (BaseFlow.driver.findElement(By.xpath("//p[@class='not-found-search']")).getText().equals("Lo sentimos, no se encontraron productos para su búsqueda: " + sku)) {
//
//					assertTrue("Lo sentimos, no se encontraron productos para su búsqueda: " + sku, false);
//				}
//
//			}
//		} catch (Exception e) {
//			log.info("Flujo de nuevo Usuario registrado , detenido en ingreso SKU por Exception: " + e);
//		}
//	}
//
//	@When("^selecciono detalles de producto_registro\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"$")
//	public void selecciono_detalles_de_producto_registro(String tipoProducto, String cantidad, String conArmado,
//			String conGarantia) throws Exception {
//		try {
//			// VALIDAR STOCK
//			if (!GenericMethods.existElement(By.xpath("//div[@class=\"out-of-stock\"]"))) {
//				p.setTipoProducto(tipoProducto);
//				p.setCantidad(FlujoCompraServices.convert(cantidad));
//				FlujoCompraServices.seleccionarColor(tipoProducto);
//				FlujoCompraServices.seleccionarTalla(tipoProducto);
//				FlujoCompraServices.seleccionarCantidad(cantidad);
//
//				FlujoCompraServices.seleccionarProductoConArmadoOrGarantia(conArmado, conGarantia, tipoProducto);
//				// TODO SE SETEA EL OBJETO PRODUCTO CON LOS DATOS DEL PRODUCTO, PRECIOS, NOMBRES
//				FlujoCompraServices.setearProducto(conArmado, conGarantia, tipoProducto);
//
//			} else {
//				throw new PSCException("PRODUCTO SIN STOCK");
//			}
//		} catch (PSCException e) {
//			log.info(e);
//			throw e;
//
//		}
//
//	}
//
//	@When("^se agrega producto al carro_registro$")
//	public void se_agrega_producto_al_carro_registro() throws Throwable {
//
//		try {
//			if (BaseFlow.isActiveSessionDriver()) {
//				GenericMethods.waitTime(BaseFlow.pageModelOrder.getPdpAgregarCarro());
//				GenericMethods.scrollClickElement(BaseFlow.driver, BaseFlow.pageModelOrder.getPdpAgregarCarro());
//			}
//		} catch (PSCException e) {
//			log.info("Flujo de nuevo Usuario registrado en checkOut, detenido en se_agrega_producto_al_carro : " + e);
//		}
//	}
//
//	@When("^Ingresar bonificacion_registro\"(.*?)\"$")
//	public void ingresar_bonificacion_registro(String agregarBonificacion) throws Throwable {
//		try {
//
//			JavascriptExecutor js = (JavascriptExecutor) BaseFlow.driver;
//			String script = "return document.getElementsByClassName('ui-dialog ui-widget ui-widget-content ui-corner-all ui-front bonus-products-modal ui-dialog-buttons').length;";
//			String existe = js.executeScript(script).toString();
//			if (!existe.equals("0")) {
//				FlujoCompraServices.productoBonificacion(agregarBonificacion);
//
//			}
//		} catch (Exception e) {
//			log.info("Flujo de Usuario registrado en checkOut, detenido en ingresar_producto_de_bonificacion : " + e);
//		}
//	}
//
//	@When("^ingreso a mini carro_registro\"(.*?)\"$")
//	public void ingreso_a_mini_carro_registro(String seleccionCarroOrPago) throws Exception {
//		try {
//			if (Constants.Browser.contains("Mobile")) {
//				GenericMethods.implicityWait(10, By.xpath("//*[@id='mini-cart']/div[1]/a/span[1]/p"));
//				WebElement btnCarro = BaseFlow.driver.findElement(By.xpath("//*[@id='mini-cart']/div[1]/a/span[1]/p"));
//				JavascriptExecutor executor = (JavascriptExecutor) BaseFlow.driver;
//				executor.executeScript("arguments[0].click();", btnCarro);
//				
//			}else {
//			GenericMethods.waitTime(BaseFlow.pageModelOrder.getPdpBtnVerCarro());
//			if (seleccionCarroOrPago.equals("comprar")) {
//				GenericMethods.scrollClickElement(BaseFlow.driver, BaseFlow.pageModelOrder.getPdpBtnComprar());
//
//			} else if (seleccionCarroOrPago.equals("carro")) {
//				GenericMethods.scrollClickElement(BaseFlow.driver, BaseFlow.pageModelOrder.getPdpBtnVerCarro());
//			} else {
//				throw new PSCException("el valor de 'seleccionCarroOrPago' es " + seleccionCarroOrPago
//						+ ", el cual no es valido de acuerdo a la tipificacion,ver en el feature");
//				}
//			}
//		} catch (PSCException e) {
//			log.error("Flujo de Usuario invitado en checkOut, detenido en ingreso a mini carro por: " + e);
//		}
//	}
//
//	@When("^ingreso al carro de compras o comprar directamente_registro\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"$")
//	public void ingreso_al_carro_de_compras_o_comprar_directamente_registro(String seleccionCarroOrPago,
//			String cgoDescuento, String tipoRegistro, String nombre, String apellido, String codTel, String telefono,
//			String tipoDocumento, String nDocumento, String usuario) throws Throwable {
//		try {
//			if (seleccionCarroOrPago.equals("comprar") && tipoRegistro.equals("checkout")) {
//				GenericMethods.waitTime(BaseFlow.pageModelOrder.getCheckoutBtnCompraGuest());
//				BaseFlow.pageModelOrder.getCheckoutBtnCompraGuest().click();
//				// INGRESO INFORMACIÓN DE REGISTRO COMO GUEST
//				FlujoCompraServices.ingresarDatosNuevoRegistro(nombre, apellido, mail, codTel, telefono, tipoDocumento,nDocumento);
//				
//				Singleton.setNombre(nombre + " " + apellido);
//				Singleton.setEmail(mail);
//				Singleton.setTelefono(codTel + telefono);
//				
//				BaseFlow.pageModelOrder.getCheckoutBtnContinuar().click();
//				
//			} else if (seleccionCarroOrPago.equals("carro") && tipoRegistro.equals("checkout")) {
//
//				// TODO CODIGO DE DESCUENTO EN %
//				if (!cgoDescuento.equals("")) {
//					BaseFlow.pageModelOrder.getCarroTxtCodigoDescuento().sendKeys(cgoDescuento);
//					BaseFlow.pageModelOrder.getCarroBtnDescuento().click();
//					// GenericMethods.implicityWait(15,
//					// By.xpath("(//span[contains(@class,'label')])[3]"));
//					if (BaseFlow.driver.findElements(By.xpath("//span[contains(@class,'bonus-item')]")).size() > 0) {
//						if (BaseFlow.driver.findElement(By.xpath("//span[contains(@class,'bonus-item')]")).getText().equals("Aplicado")) {
//
//							if (BaseFlow.driver.findElements(By.xpath("(//span[contains(@class,'label')])[3]")).size() > 0) {
//								String descuentoCodigo = BaseFlow.driver.findElement(By.xpath("(//span[contains(@class,'label')])[3]")).getText().replaceAll("\\D+", "");
//								if (descuentoCodigo != "") {
//									Singleton.setDescuentoPorCdgo(Integer.parseInt(descuentoCodigo));
//									System.out.println("% de Descuento: " + Integer.parseInt(descuentoCodigo));
//								}
//
//							}
//
//						}
//					}
//
//				}
//
//				WebElement irACaja = GenericMethods.implicityWait(5, By.name("dwfrm_cart_checkoutCart"));
//				GenericMethods.scrollClickElement(BaseFlow.driver, irACaja);
//				// GenericMethods.waitTime(BaseFlow.pageModel.getCarroBtnIrALaCaja());
//				// BaseFlow.pageModel.getCarroBtnIrALaCaja().click();
//				GenericMethods.waitTime(BaseFlow.pageModelOrder.getCheckoutBtnCompraGuest());
//				BaseFlow.pageModelOrder.getCheckoutBtnCompraGuest().click();
//				FlujoCompraServices.ingresarDatosNuevoRegistro(nombre, apellido, mail, codTel, telefono, tipoDocumento,nDocumento);
//				
//				Singleton.setNombre(nombre + " " + apellido);
//				Singleton.setEmail(mail);
//				Singleton.setTelefono(codTel + telefono);
//				
//				BaseFlow.pageModelOrder.getCheckoutBtnContinuar().click();
//			} else if (seleccionCarroOrPago.equals("carro") && tipoRegistro.equals("home")) {
//
//				if (!cgoDescuento.equals("")) {
//					BaseFlow.pageModelOrder.getCarroTxtCodigoDescuento().sendKeys(cgoDescuento);
//					BaseFlow.pageModelOrder.getCarroBtnDescuento().click();
//					// GenericMethods.implicityWait(15,
//					// By.xpath("(//span[contains(@class,'label')])[3]"));
//					if (BaseFlow.driver.findElements(By.xpath("//span[contains(@class,'bonus-item')]")).size() > 0) {
//						if (BaseFlow.driver.findElement(By.xpath("//span[contains(@class,'bonus-item')]")).getText().equals("Aplicado")) {
//
//							if (BaseFlow.driver.findElements(By.xpath("(//span[contains(@class,'label')])[3]")).size() > 0) {
//								String descuentoCodigo = BaseFlow.driver.findElement(By.xpath("(//span[contains(@class,'label')])[3]")).getText().replaceAll("\\D+", "");
//								if (descuentoCodigo != "") {
//									Singleton.setDescuentoPorCdgo(Integer.parseInt(descuentoCodigo));
//									System.out.println("% de Descuento: " + Integer.parseInt(descuentoCodigo));
//								}
//
//							}
//
//						}
//					}
//
//				}
//
//				WebElement irACaja = GenericMethods.implicityWait(5, By.name("dwfrm_cart_checkoutCart"));
//				GenericMethods.scrollClickElement(BaseFlow.driver, irACaja);
//				// GenericMethods.waitTime(BaseFlow.pageModelOrder.getCarroBtnIrALaCaja());
//				// GenericMethods.captureBitmap();
//				// BaseFlow.pageModelOrder.getCarroBtnIrALaCaja().click();
//			}
//		} catch (Exception e) {
//			log.info("Flujo de nuevo Usuario registrado en checkOut, detenido en ingreso_a_pago_automatico : " + e);
//		}
//	}
//
//	@When("^ingresar tipo de entrega_registro\"(.*?)\"\"(.*?)\" \"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"$")
//	public void ingresar_tipo_de_entrega_registro(String tipoEntrega, String tipoUbicacion, String ubicacion,
//			String direccion, String numeroCasaDepto, String calle, String cdgPostal, String numeroCalle, String region,
//			String comuna) throws Throwable {
//		try {
//
//			FlujoCompraServices.ingresoDatosDespachoOrTiendaOrder(tipoEntrega, tipoUbicacion, ubicacion, direccion,numeroCasaDepto, cdgPostal, region, comuna, calle, numeroCalle);
//
//			if (tipoEntrega.equals(Constants.TIPODESPACHO_RETIRO_EN_TIENDA)	|| tipoEntrega.equals(Constants.TIPODESPACHO_RETIRO_EN_TIENDA_TERCERO)) {
//
//				Singleton.setDireccion(BaseFlow.driver.findElement(By.xpath("(//ul[@class='list-data'])[2]/li[2]")).getText().split(": ")[1]);
//				Singleton.setTipoEntrega(tipoEntrega);
//
//			} else {
//				Singleton.setDireccion(calle + " " + numeroCalle + ", " + numeroCasaDepto + ", " + comuna + ", " + region);
//				Singleton.setTipoEntrega(tipoEntrega);
//			}
//			
//			Singleton.setFechaDespachoRetiro(BaseFlow.driver.findElement(By.xpath("//div[@class='expected-delivery-date']/strong")).getText());
//			
//			
//		} catch (Exception e) {
//			log.error("Flujo detenido en ingreso datos despacho o tienda : " + e);
//		}
//	}
//
//	@When("^seleccionar tipo de despacho_registro\"(.*?)\"$")
//	public void seleccionar_tipo_de_despacho_registro(String tipoDespacho) throws Throwable {
//		try {
//			
//			String infoDespacho[] =FlujoCompraServices.seleccionarTipoDespacho(tipoDespacho);
//			String tipodeDespacho = infoDespacho[0];
//			System.out.println(tipodeDespacho);
//			Singleton.setTipoDespacho(tipodeDespacho);
//			int costoDespacho = FlujoCompraServices.convert(infoDespacho[1]);
//			System.out.println(costoDespacho);
//			Singleton.setCostoDespacho(costoDespacho);
//
//		} catch (Exception e) {
//			log.info("Caida en : seleccionar tipo de Despacho ", e);
//			throw e;
//		}
//	}
//
//	@When("^ingreso compra con factura_registro\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"$")
//	public void ingreso_compra_con_factura_registro(String ingresaFactura, String nomEmpresa, String razonSocial,
//			String regionFactura, String calleFactura, String numCasaDeptoFactura, String personaContactoFactura,
//			String rutFactura, String giro, String comunaFactura, String numeroFactura, String fonoFactura,
//			String emailFactura) throws Exception {
//		try {
//			if (ingresaFactura.equalsIgnoreCase("true")) {
//				BaseFlow.pageModelOrder.getCheckoutBtnIngresaDatosFactura().click();
//				FlujoCompraServices.ingresoDatosFactura(nomEmpresa, razonSocial, regionFactura, calleFactura,numCasaDeptoFactura, personaContactoFactura, rutFactura, giro, comunaFactura, numeroFactura,fonoFactura, emailFactura);
//				// BaseFlow.pageModelOrder.getCheckoutBtnIngresaDatosFactura().click();
//			}
//		} catch (Exception e) {
//			log.error("Flujo detenido en ingreso compra con factura_registro : " + e);
//		}
//	}
//
//	@When("^ingreso codigo novios_registro\"(.*?)\"$")
//	public void ingreso_codigo_novios_registro(String codigoNovios) throws Throwable {
//
//		try {
//			if (!codigoNovios.equals("")) {
//				BaseFlow.pageModel.getCheckOutBtnNovios().click();
//				BaseFlow.pageModel.getCheckOutTxtCodigoNovios().sendKeys(codigoNovios);
//				BaseFlow.pageModel.getCheckOutTxtCodigoNovios().sendKeys(Keys.TAB);
//				while (BaseFlow.driver.findElements(By.xpath("(//div[@class='buttons']/button)[1]")).size() > 0) {
//					GenericMethods.scrollElement(BaseFlow.driver, BaseFlow.pageModel.getCheckOutBtnAssociatePoints());
//					BaseFlow.pageModel.getCheckOutBtnAssociatePoints().click();
//				}
//
//			}
//
//		} catch (Exception e) {
//			log.error("Flujo detenido en ingreso Código de Novios : " + e);
//		}
//
//	}
//
//	@When("^selecciono_metodo_pago_registro\"(.*?)\"$")
//	public void selecciono_metodo_pago_registro(String metodoPago) throws Throwable {
//		try {
//			// TODO refactorizar a switch
//			if (metodoPago.equals(Constants.TARJETA_CREDITO_CENCOSUD)) {
//				GenericMethods.scrollElement(BaseFlow.driver, BaseFlow.pageModelOrder.getCheckOutHrefCencosudCredito());
//				GenericMethods.waitTime(BaseFlow.pageModelOrder.getCheckOutHrefCencosudCredito());
//				BaseFlow.pageModelOrder.getCheckOutHrefCencosudCredito().click();
//			} else if (metodoPago.equals(Constants.TARJETA_CREDITO)) {
//				GenericMethods.waitTime(BaseFlow.pageModelOrder.getCheckOutHrefTarjetaCredito());
//				GenericMethods.scrollElement(BaseFlow.driver, BaseFlow.pageModelOrder.getCheckOutHrefTarjetaCredito());
//				BaseFlow.pageModelOrder.getCheckOutHrefTarjetaCredito().click();
//			} else if (metodoPago.equals(Constants.REDCOMPRA)) {
//				GenericMethods.scrollElement(BaseFlow.driver, BaseFlow.pageModelOrder.getCheckOutHrefRedCompra());
//				GenericMethods.waitTime(BaseFlow.pageModelOrder.getCheckOutHrefRedCompra());
//				BaseFlow.pageModelOrder.getCheckOutHrefRedCompra().click();
//			} else if (metodoPago.equals(Constants.TMASCARD)) {
//				GenericMethods.scrollElement(BaseFlow.driver, BaseFlow.pageModelOrder.getCheckOutHrefTmas());
//				GenericMethods.waitTime(BaseFlow.pageModelOrder.getCheckOutHrefTmas());
//				BaseFlow.pageModelOrder.getCheckOutHrefTmas().click();
//			}
//		} catch (Exception e) {
//			log.error("Flujo detenido en seleccion metodo de pago Nuevo Registro: " + e);
//		}
//
//	}
//
//	@When("^ingreso datos metodo pago_registro\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"\"(.*?)\"$")
//	public void ingreso_datos_metodo_pago_registro(String metodoPago, String codigoGiftCard, String claveGiftCard,
//			String nombreTitularTarjeta, String apellidoTitularTarjeta, String numeroTarjeta, String mesTarjeta,
//			String anioTarjeta, String cvvTarjeta, String nCuotas, String guardarTarjeta, String TipoTarjetaCenco,
//			String rut, String claveRedCompra) throws Throwable {
//
//		try {
//			// FlujoCompraServices.ingresarDatosMetodosPago(metodoPago, numeroTarjeta,
//			// mesTarjeta, anioTarjeta,cvvTarjeta,nombreTitularTarjeta,
//			// apellidoTitularTarjeta,rut,claveRedCompra);
//			FlujoCompraServices.ingresarDatosMetodosPagoOrder(metodoPago, codigoGiftCard, claveGiftCard, numeroTarjeta,	mesTarjeta, anioTarjeta, cvvTarjeta, nombreTitularTarjeta, apellidoTitularTarjeta, rut,claveRedCompra);
//			// VALIDO PAGO
//			if (BaseFlow.driver.findElements(By.xpath("//div[@class='error-form']")).size() > 0) {
//				// assertTrue("Error en el Pago", false);
//				System.out.println(BaseFlow.driver.findElement(By.xpath("//div[@class='error-form']")).getText());
//			}
//			if (BaseFlow.driver.findElements(By.xpath("//div[@class='box-error']")).size() > 0) {
//				assertTrue(BaseFlow.driver.findElement(By.xpath("//div[@class='box-error']/h2")).getText(), false);
//				System.out.println(BaseFlow.driver.findElement(By.xpath("//div[@class='box-error']/h2")).getText());
//
//			}
//
//			Singleton.setnTarjeta(numeroTarjeta);
//			System.out.println("Número de Cuotas: " +Singleton.getnCuotas());
//			System.out.println("Número de Tarjeta: "+Singleton.getnTarjeta());
//
//			// GenericMethods.waitTime(BaseFlow.pageModelOrder.getCheckoutBtnRealizarPedido());
//			// GenericMethods.scrollElement(BaseFlow.driver,
//			// BaseFlow.pageModelOrder.getCheckoutBtnRealizarPedido());
//			// BaseFlow.pageModelOrder.getCheckoutBtnRealizarPedido().click();
//
//		} catch (Exception e) {
//			log.error("Flujo detenido en ingreso datos metodo pago : " + e);
//		}
//	}
//
//	@When("^ingreso clave de registro_registro\"(.*?)\"\"(.*?)\"$")
//	public void ingreso_clave_de_registro_registro(String clave, String tipoRegistro) throws Throwable {
//		try {
//			FlujoCompraServices.registroNuevoUsuario(clave, tipoRegistro);
//		} catch (Exception e) {
//			log.info("Flujo de nuevo Usuario registrado en checkOut, detenido en ingresar clave de registro : " + e);
//		}
//	}
//
//	@Then("^verifico que se genere la orden de compra_registro$")
//	public void verifico_que_se_genere_la_orden_de_compra_registro() throws Throwable {
//
//		try {
//			element = GenericMethods.implicityWait(30, By.id("wrapper"));
//			// GenericMethods.implicityWait(30,By.xpath("//div[@class='pt_order-confirmation
//			// container']"));
//			String numOC = BaseFlow.pageModel.getCheckoutDivNumOrdenCompra().getText().toString();
//			//TODO GENERACIÓN DE numOC SE INGRESA EN EL EXCEL
//			 ManagementMicrosoftService.WriteNumeroOCToExcelFile(nombreHoja, 0, numOC);
//
//			log.info("La OC se ha generado correctamente : "+ BaseFlow.pageModelOrder.getCheckoutDivNumOrdenCompra().getText().toString());
//		} catch (Exception e) {
//			element = null;
//			assertTrue("Error no se ha generado la OC ", false);
//			log.info("error al generar la OC" + e);
//		}
//	}
//
//	// ###################### PRUEBA DE COMPONENTE SE VALIDA EL DESPLIEGUE DE LA
//	// ORDER CONFIRMATION ###########################33
//
//	@Then("^Valido que se despligue el Header OrderConfirmation$")
//	public void valido_que_se_despligue_el_Header_OrderConfirmation() throws Throwable {
//
//		try {
//			if (element != null) {
//				if (GenericMethods.existElement(By.xpath("(//div[@class='CA-checkout-header'])"))) {
//
//					if (GenericMethods.existElement(By.xpath("//div[@class='col-xs-12 col-sm-2 col-md-2 col-lg-1']/a/img"))) {
//						assertTrue("No se despliega el logo de Paris ",GenericMethods.existElement(By.xpath("//div[@class='col-xs-12 col-sm-2 col-md-2 col-lg-1']/a/img")));
//						assertTrue("el link al Home del icono no responde", ComponentUtil.sendGet1(BaseFlow.driver.findElement(By.xpath("//div[@class='col-xs-12 col-sm-2 col-md-2 col-lg-1']/a"))));
//						// String href =
//						// BaseFlow.driver.findElement(By.xpath("//div[@class='CA-checkout-header']/div[1]/a")).getAttribute("href");
//						assertTrue("No se despliega la imagen del icono Paris al home",	ComponentUtil.sendGetsrc(BaseFlow.driver.findElement(By.xpath("//div[@class='col-xs-12 col-sm-2 col-md-2 col-lg-1']/a/img"))));
//
//					} else {
//						assertTrue("No Se despliega el icono de Paris", false);
//					}
//
//					if (GenericMethods.existElement(By.xpath("//div[@class='col-sm-10 col-md-10 col-lg-11 text-right hidden-xs']/h4"))) {
//						assertTrue("No se despliega el mensaje de Necesitas Ayuda en tu compra",BaseFlow.driver.findElement(By.xpath("//div[@class='col-sm-10 col-md-10 col-lg-11 text-right hidden-xs']/h4")).getText().equals("¿Necesitas ayuda en esta compra?"));
//						assertTrue("No se despliega el texto Llámanos al 600 400 8000 ", (BaseFlow.driver.findElement(By.xpath("//div[@class='col-sm-10 col-md-10 col-lg-11 text-right hidden-xs']/p")).getText().equals("Llámanos al 600 400 8000 y un ejecutivo te ayudará en la compra.")));
//					} else {
//						assertTrue("no se despliega el mensaje de ¿Necesitas ayuda en esta compra?", false);
//					}
//
//				} else {
//					assertTrue("No se despliega el header de OrderConfirmation", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege del Header OrderConfirmation : " + e);
//		}
//
//	}
//
//	@Then("^Valido que se despliege mensaje de exito$")
//	public void valido_que_se_despliege_mensaje_de_exito() throws Throwable {
//
//		try {
//
//			if (element != null) {
//				GenericMethods.implicityWait(10,By.xpath("//div[@class='box-checkout-confirmation-success text-center']"));
//				if (GenericMethods.existElement(By.xpath("//div[@class='box-checkout-confirmation-success text-center']"))) {
//					if (GenericMethods.existElement(By.xpath("//div[@class='box-checkout-confirmation-success text-center']//strong"))) {
//						// SE MODIFICA MENSAJE DE Su a Tu
//						// assertTrue("No se despliega el mensaje ¡Su compra se completó exitosamente!",
//						// BaseFlow.driver
//						// .findElement(By.xpath("//div[@class='box-checkout-confirmation-success
//						// text-center']//strong"))
//						// .getText().equals("¡Su compra se completó exitosamente!"));
//
//						assertTrue("No se despliega el mensaje ¡Su compra se completó exitosamente!",BaseFlow.driver.findElement(By.xpath("//div[@class='box-checkout-confirmation-success text-center']//strong")).getText().equals("¡Tu compra se completó exitosamente!"));
//
//					} else {
//						assertTrue("No se despliega el componenete del mensaje de éxito", false);
//					}
//
//				} else {
//					assertTrue("No se despliega contenedor de Mensaje de Éxito", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege mensaje de éxito : " + e);
//		}
//	}
//
//	@Then("^Valido que se despliege mensaje de envio al mail de confirmacion$")
//	public void valido_que_se_despliege_mensaje_de_envio_al_mail_de_confirmacion() throws Throwable {
//
//		try {
//
//			if (element != null) {
//				if (Singleton.getTipoEntrega().equals("domicilio")) {
//
//					if (GenericMethods.existElement(By.xpath("//div[@class='confirmation-shipping-message text-center']"))) {
//						if (GenericMethods.existElement(By.xpath("//div[@class='confirmation-shipping-message text-center']/p"))) {
//							assertTrue("Tu producto será enviado a la dirección proporcionada. La confirmación del pedido serán enviadas a tu Email.",BaseFlow.driver.findElement(By.xpath("//div[@class='confirmation-shipping-message text-center']/p")).getText().equals("Tu producto será despachado a la dirección indicada. La confirmación del pedido será enviada a tu Email."));
//							// TODO se modifica texto Tu producto será enviado a la dirección proporcionada.
//							// La confirmación del pedido serán enviadas a tu Email. por Tu producto será
//							// despachado a la dirección indicada. La confirmación del pedido será enviada a
//							// tu Email.
//						} else {
//							assertTrue("No se despliega el componente del mensaje de confirmación de envio email",false);
//						}
//
//					} else {
//						assertTrue("No se despliega contenedor de Mensaje de confirmación del email", false);
//					}
//				} else if (Singleton.getTipoEntrega().equals("retiroEnTienda") || Singleton.getTipoEntrega().equals("retiroEnTiendaTercero")) {
//
//					if (GenericMethods.existElement(By.xpath("//div[@class='presales-order-confirmation-message text-center']"))) {
//						if (GenericMethods.existElement(By.xpath("//div[@class='presales-order-confirmation-message text-center']/p"))) {
//							assertTrue("No se despliega el mensaje para Retiro en Tienda :Tu pedido será entregado en la tienda seleccionada. Se te ha enviado un Email con la confirmación de tu pedido y los pasos a seguir. ",BaseFlow.driver.findElement(By.xpath("//div[@class='presales-order-confirmation-message text-center']/p")).getText().equals("Tu pedido será entregado en la tienda seleccionada. Se te ha enviado un Email con la confirmación de tu pedido y los pasos a seguir."));
//						} else {
//							assertTrue("No se despliega el componente del mensaje de confirmación del Retiro en tienda",false);
//						}
//
//					} else {
//						assertTrue("No se despliega contenedor de Mensaje de confirmación del email", false);
//					}
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Validación mensaje de envio a correo : " + e);
//		}
//	}
//
//	@Then("^Valido que se despligue el Footer OrderConfirmation$")
//	public void valido_que_se_despligue_el_Footer_OrderConfirmation() throws Throwable {
//
//		try {
//			if (element != null) {
//
//				if (GenericMethods.existElement(By.xpath("(//div[@class='footer confirmation'])"))) {
//					if (GenericMethods.existElement(By.xpath("//div[@class='CA-checkout-footer']"))) {
//						assertTrue("No se despliega el mensaje 2018 - Todos los derechos reservados Paris.cl",BaseFlow.driver.findElement(By.xpath("//div[@class='CA-checkout-footer']")).getText()
//										.equals("Todos los derechos reservados Paris.cl - Al comprar aceptas los Términos y condiciones de Paris.cl"));
//					} else {
//						assertTrue("No se despliega el Footer", false);
//					}
//
//				} else {
//					assertTrue("No se despliega el footer", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Validación Despliege Footer en la OrderConfirmation : " + e);
//		}
//	}
//
//	@Then("^Valido que se despliegue el numero de orden$")
//	public void valido_que_se_despliegue_el_numero_de_orden() throws Throwable {
//		try {
//
//			if (element != null) {
//				if (GenericMethods.existElement(By.xpath("(//h2[@class='title text-uppercase'])[1]"))) {
//					assertTrue("No se despliega el texto header de DETALLES DE TU ORDEN DE COMPRA",BaseFlow.driver.findElement(By.xpath("(//h2[@class='title text-uppercase'])[1]")).getText().equals("DETALLES DE TU ORDEN DE COMPRA")); // TODO antes aparecia ID DE COMPRA
//																				// ,AHORA SE MODIFICO POR Detalles de tu
//																				// orden de compra
//				} else {
//					assertTrue("No se despliega el header de Detalles de tu orden de compra ", false);
//				}
//				if (GenericMethods.existElement(By.xpath("(//dt[@class='text-right'])[1]/strong"))) {
//					assertTrue("No se despliega el Texto Número de orden",BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[1]/strong")).getText().equals("Número de orden:"));
//
//				} else {
//					assertTrue("No existe el texto Número de orden", false);
//				}
//				if (GenericMethods.existElement(By.xpath("(//dt[@class='text-right'])[1]/following::dd[1]"))) {
//					assertTrue("No se despliega en numero de la orden",	!BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[1]/following::dd[1]"))
//									.getText().equals(""));
//					String norden = BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[1]/following::dd[1]")).getText();
//					System.out.println("Número de Orden: "+norden);
//					Singleton.setNumeroOrden(norden);
//				} else {
//					assertTrue("No existe el componente de numero de orden", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege Numero de Orden : " + e);
//		}
//	}
//
//	@Then("^valido que se despliegue el nombre del comprado$")
//	public void valido_que_se_despliegue_el_nombre_del_comprado() throws Throwable {
//		try {
//			if (element != null) {
//
//				if (GenericMethods.existElement(By.xpath("(//dt[@class='text-right'])[2]/strong"))) {
//					assertTrue("No se despliega el texto Comprador",BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[2]/strong")).getText().equals("Comprador"));
//
//				} else {
//					assertTrue("No existe el componente de Comprador", false);
//				}
//				if (GenericMethods.existElement(By.xpath("(//dt[@class='text-right'])[2]/following::dd[1]"))) {
//					// comparo el nombre de la orden con el del singleton
//					assertEquals("No se despliega el nombre del comprador", Singleton.getNombre(), BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[2]/following::dd[1]")).getText());
//				} else {
//					assertTrue("No se despliega el componente del nombre del comprador", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege Nombre del Comprador : " + e);
//		}
//	}
//
//	@Then("^valido numero de contacto comprador$")
//	public void valido_numero_de_contacto_comprador() throws Throwable {
//		try {
//			if (element != null) {
//
//				if (GenericMethods.existElement(By.xpath("(//dt[@class='text-right'])[3]/strong"))) {
//					assertTrue("No se despliega el texto Teléfono",BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[3]/strong")).getText().equals("Teléfono:"));
//
//				} else {
//					assertTrue("No existe el componente de Teléfono", false);
//				}
//				if (GenericMethods.existElement(By.xpath("(//dt[@class='text-right'])[2]/following::dd[2]"))) {
//					//VALIDO TELEFONO DESPLEGADO CON EL SINGLETON
//					assertEquals("No se despliega el telefono del comprador", Singleton.getTelefono(), BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[2]/following::dd[2]")).getText());
//				} else {
//					assertTrue("No se despliega el componente del telefono del comprador", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege número de contacto del Comprador : " + e);
//		}
//	}
//
//	@Then("^valido email desplegado$")
//	public void valido_email_desplegado() throws Throwable {
//		try {
//			if (element != null) {
//
//				if (GenericMethods.existElement(By.xpath("(//dt[@class='text-right'])[4]/strong"))) {
//					assertTrue("No se despliega el texto Email de contacto",BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[4]/strong")).getText()
//									.equals("Email de Contacto")); // TODO se reemplaza E-mail de contacto por Email de
//																	// contacto
//				} else {
//					assertTrue("No existe el componente de Email de contacto", false);
//				}
//				if (GenericMethods.existElement(By.xpath("(//dt[@class='text-right'])[2]/following::dd[3]"))) {
//					// VALIDO EMAIL DESPLEGADO CON EL SINGLETON
//					assertEquals("No se despliega el email del comprador", Singleton.getEmail(), BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[2]/following::dd[3]")).getText());
//				} else {
//					assertTrue("No se despliega el componente del email del comprador", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege de Email : " + e);
//		}
//	}
//
//	@Then("^valido dirección desplegada$")
//	public void valido_dirección_desplegada() throws Throwable {
//		try {
//			if (element != null) {
//
//				if (GenericMethods.existElement(By.xpath("(//h2[@class='title text-uppercase'])[2]"))) {
//					// TODO se reemplaza DETALLE DE COMPRA por DETALLES DE TU COMPRA
//					assertTrue("No se despliega el texto header de DETALLES DE COMPRA",	BaseFlow.driver.findElement(By.xpath("(//h2[@class='title text-uppercase'])[2]")).getText().equals("DETALLES DE TU COMPRA")); 
//																		
//				} else {
//					assertTrue("No se despliega el Header de DETALLES DE TU COMPRA", false);
//				}
//				if (GenericMethods.existElement(By.xpath("(//dt[@class='text-right'])[6]/strong"))) {
//					assertTrue("No se despliega el Texto Dirección",BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[6]/strong")).getText().equals("Dirección:"));
//
//				} else {
//					assertTrue("No existe el texto Dirección", false);
//				}
//				if (GenericMethods.existElement(By.xpath("(//dt[@class='text-right'])[1]/following::dd[6]")) && !Singleton.getTipoEntrega().equals(Constants.TIPODESPACHO_RETIRO_EN_TIENDA)) {
//
//					// VALIDO DIRECCION DESPLEGADA CON EL SINGLETON
//					assertEquals("No se despliega la direccion", Singleton.getDireccion(), BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[1]/following::dd[6]")).getText());
//				} else if (Singleton.getTipoEntrega().equals(Constants.TIPODESPACHO_RETIRO_EN_TIENDA)) {
//					assertEquals("No se despliega la direccion de la Tienda de Retiro", Singleton.getDireccion(),BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[1]/following::dd[6]")).getText());
//				} else {
//					assertTrue("No existe el componente de dirección", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege de Dirección  : " + e);
//		}
//	}
//
//	@Then("^Validar que se despliegue nombre de Retiro Tercero$")
//	public void validar_que_se_despliegue_nombre_de_Retiro_Tercero() throws Throwable {
//		try {
//			if (element != null) {
//
//				if (Singleton.getTipoEntrega().equals("retiroEnTiendaTercero")) {
//					// TODO generar en el singleton la información del tercero (nombre, apellido , rut
//					assertTrue("No se despliega el label :Nombre de la persona que retira:",BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[8]/strong")).getText().equals("Nombre de la persona que retira:"));
//					assertTrue("No se despliega el label :RUT de la persona que retira:",BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[9]/strong")).getText().equals("RUT de la persona que retira:"));
//					assertTrue("El rut del Tercero de Retiro en Tienda no es consistente",BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[9]/following::dd[1]")).getText().equals(Singleton.getRutTercero()));
//					assertTrue("El Nombre del Tercero de Retiro en Tienda no es consistente",BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[8]/following::dd[1]")).getText().equals(Singleton.getNombreTercero()));
//
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en validación de datos de Retiro de Tercero" + e);
//		}
//	}
//
//	@Then("^valido metodo de despacho desplegado$")
//	public void valido_metodo_de_despacho_desplegado() throws Throwable {
//		try {
//			if (element != null) {
//
//				if (GenericMethods.existElement(By.xpath("(//dt[@class='text-right'])[5]/strong"))) {
//					 // TODO se modifica Método: por Método de entrega:
//					assertTrue("No se despliega el texto Método",BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[5]/strong")).getText().equals("Método de entrega:"));
//
//				} else {
//					assertTrue("No existe el componente de Método de Despacho", false);
//				}
//				if (GenericMethods.existElement(By.xpath("(//dt[@class='text-right'])[2]/following::dd[4]"))) {
//
//					// TODO valido los tipos de despacho para el caso tipoEntrega = domicilio , y en
//					// caso de Retiro en Tienda valido que indique Collect from Store se modifico
//					// por Retiro en tienda
//					if (Singleton.getTipoEntrega().equals(Constants.TIPODESPACHO_DOMICILIO)) {
//						assertEquals("No se despliega el  método de despacho del comprador",Singleton.getTipoDespacho(),BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[2]/following::dd[4]")).getText());
//					} else {
//						// en caso de RetiroenTienda se debe desplega Collect from Store , se cambio por
//						// Retiro en tienda
//						assertEquals("No se despliega el  método de despacho Retiro en tienda del comprador","Retiro en tienda",BaseFlow.driver.findElement(By.xpath("(//dt[@class='text-right'])[2]/following::dd[4]")).getText());
//					}
//
//				} else {
//					assertTrue("No se despliega el método de despacho comprador", false);
//				}
//				// log.info("captura metodo de despacho desplegado ");
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege de Método de Despacho : " + e);
//		}
//	}
//
//	@Then("^valido despliegue de productos$")
//	public void valido_despliegue_de_productos() throws Throwable {
//		try {
//			if (element != null) {
//
//				System.out.println(Singleton.getProducto().toString());
//				FlujoCompraServices.validarProductosOrder();
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege de productos en la Order Confirmation : " + e);
//		}
//
//	}
//
//	@Then("^valido despliegue de fecha de Retiro$")
//	public void valido_despliegue_de_fecha_de_Retiro() throws Throwable {
//	try {
//		
//		if (Singleton.getFechaDespachoRetiro().equals(BaseFlow.driver.findElement(By.xpath("//div[@class='expected-delivery-date']/strong")).getText())) {
//			assertTrue("Se despliega correctamente la fecha de Despacho o Retiro", true);
//		} else {
//			
//			assertTrue("La fecha de Retiro o Despacho no es consistente",false);
//		}
//		
//	} catch (Exception e) {
//		log.info("Flujo Nuevo Registro error en Valido Despliege de Fecha para retiro en Tienda en la Order Confirmation : " + e);
//	}
//	}
//	
//	
//	
//	
//	@Then("^valido que se despliega el descuento$")
//	public void valido_que_se_despliega_el_descuento() throws Throwable {
//		try {
//			if (element != null) {
//
//				if (Singleton.getDescuentoPorCdgo() != 0) {
//					if (BaseFlow.driver.findElements(By.xpath("div[@class='box-t-carro-uni box-total-mc box-total-os order-discount']")).size() > 0) {
//
//						if (BaseFlow.driver.findElements(By.xpath("//div[@class='box-t-carro-uni box-total-mc box-total-os order-discount']/p/span")).size() > 0) {
//							assertTrue("No se Despliega el label Descuento de la orden:", BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni box-total-mc box-total-os order-discount']/p/span")).getText().equals("Descuento de la orden:"));
//
//							if (BaseFlow.driver.findElements(By.xpath("//div[@class='box-t-carro-uni box-total-mc box-total-os order-discount']/p/strong")).size() > 0) {
//								Integer descuento = FlujoCompraServices.convert(BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni box-total-mc box-total-os order-discount']/p/strong")).getText());
//								System.out.println("Descuento Desplegado: " + descuento + "Descuento Aplicado: "+ Singleton.getDescuentoPorCdgo());
//								assertTrue("El descuento no es consistente con el aplicado",FlujoCompraServices.convert(BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni box-total-mc box-total-os order-discount']/p/strong")).getText()).equals(Singleton.getDescuentoPorCdgo()));
//
//							} else {
//								assertTrue("No se despliega el monto de Descuento", false);
//							}
//
//						} else {
//							assertTrue("No se despliega el componente del label de Descuento", false);
//						}
//
//					}
//
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege de Descuento Aplicado : " + e);
//		}
//	}
//
//	@Then("^valido que se despliega el total de la compra$")
//	public void valido_que_se_despliega_el_total_de_la_compra() throws Throwable {
//		try {
//			if (element != null) {
//
//				// SE VALIDA EL DESPLIEGUE DEL TOTAL DE LA SUMA DE LOS PRODUCTOS , NO SE
//				// DESPLEGARA SEGUN NUEVO CR 27082018
//				if (GenericMethods.existElement(By.xpath("//td[@class='text-right']"))) {
//					assertTrue("No se despliega el texto Pago Total", BaseFlow.driver.findElement(By.xpath("//td[@class='text-right']/strong")).getText().equals("Pago Total:"));
//					String p_total = BaseFlow.driver.findElement(By.xpath("//td[@class='text-right']")).getText().replaceAll("Pago Total: ", "");
//
//					if (p_total != null) {
//						// FlujoCompraServices.convert(p_total);
//						Singleton.setTotal(FlujoCompraServices.convert(p_total));
//						System.out.println("Total de los productos Front: " + Singleton.getTotal());
//
//					} else {
//						assertTrue("No se despliega el Monto Total del orderdetail", false);
//					}
//					//
//				} else {
//					assertTrue("No existe el componente que contiene el monto Total", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege del Total de la suma de los Productos  : " + e);
//		}
//
//	}
//
//	@Then("^valido que se despliege el subtotal de la orden$")
//	public void valido_que_se_despliege_el_subtotal_de_la_orden() throws Throwable {
//
//		try {
//			if (element != null) {
//
//				if (GenericMethods.existElement(By.xpath("//div[@class='box-t-carro-uni box-total-mc box-total-os']"))) {
//					assertEquals("No se despliega el texto SubTotal de orden", "Subtotal:", BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni box-total-mc box-total-os']/p/span")).getText());
//					if (GenericMethods.existElement(By.xpath("//div[@class='box-t-carro-uni box-total-mc box-total-os']/p/strong"))) {
//						String sub_total = BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni box-total-mc box-total-os']/p/strong")).getText();
//						if (sub_total != null) {
//							// FlujoCompraServices.convert(sub_total);
//							assertTrue("El Subtotal del Front Order Confirmation: " + sub_total	+ " no es consistente con el Calculado: " + Singleton.getSubTotal(),
//									FlujoCompraServices.convert(sub_total).equals(Singleton.getSubTotal()));
//							// System.out.println("subTotal Back: "+ Singleton.getSubTotal());
//							// System.out.println("subTotal de Orden Front:" +
//							// FlujoCompraServices.convert(sub_total));
//						} else {
//							assertTrue("no se despliega el Subtotal en el método de Pago", false);
//						}
//
//					} else {
//						assertTrue("No se despliega el monto del Subtotal de la orden en el metodo de pago", false);
//					}
//
//				} else {
//					assertTrue("No se despliega el componente Subtotal de Orden", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege Subtotal de la Orden  : " + e);
//		}
//	}
//
//	@Then("^valido el total del pedido$")
//	public void valido_el_total_del_pedido() throws Throwable {
//		try {
//			if (element != null) {
//
//				if (GenericMethods.existElement(By.xpath("//div[@class='box-total-mc box-total-os box-t-carro-uni order-total']"))) {
//					assertEquals("No se despliega el texto Total del Pedido", "Total:",	BaseFlow.driver.findElement(By.xpath("//div[@class='box-total-mc box-total-os box-t-carro-uni order-total']/h4/p/span")).getText());
//					if (GenericMethods.existElement(By.xpath("//div[@class='box-total-mc box-total-os box-t-carro-uni order-total']/h4/p/strong"))) {
//						String totalPedidoFront = BaseFlow.driver.findElement(By.xpath("//div[@class='box-total-mc box-total-os box-t-carro-uni order-total']/h4/p/strong")).getText();
//						if (totalPedidoFront != null) {
//							// FlujoCompraServices.convert(sub_total);
//							// ####################################################################################################################################3
//							// Comprar con el Singleton.totalPedido();= totalPedidoFront
//							assertTrue("Total Pedido del Order Confirmation:"+ FlujoCompraServices.convert(totalPedidoFront) + " No es Consistente con el Calculado: " + Singleton.getTotalPedido(),
//									FlujoCompraServices.convert(totalPedidoFront).equals(Singleton.getTotalPedido()));
//
//							// System.out.println("Total del Pedido Front: " + Singleton.getTotalPedido());
//						} else {
//							assertTrue("no se despliega el Total del Pedido en el método de Pago", false);
//						}
//
//					} else {
//						assertTrue("No se despliega el monto Total del Pedido de la orden en el metodo de pago", false);
//					}
//
//				} else {
//					assertTrue("No se despliega el componente Total del Pedido", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege del Total del Pedido de la Order  : " + e);
//		}
//	}
//
//	@Then("^valido que se despliegue el costo de envio$")
//	public void valido_que_se_despliegue_el_costo_de_envio() throws Throwable {
//		try {
//			if (element != null) {
//
//				if (GenericMethods.existElement(By.xpath("//div[@class='box-t-carro-uni box-total-mc box-total-os shipping-cost']"))) {
//					// validar con singleton Singleton.getTipoDespacho();
//					assertTrue("No se despliega el texto de tipo de despacho",!BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni box-total-mc box-total-os shipping-cost']/p/span")).getText().equals(""));
//					if (GenericMethods.existElement(By.xpath("//div[@class='box-t-carro-uni box-total-mc box-total-os shipping-cost']/p/strong"))) {
//						String costoDespacho = BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni box-total-mc box-total-os shipping-cost']/p/strong")).getText();
//						if (!costoDespacho.equals("Free")) {
//							if (Singleton.getTipoDespacho().equals(Constants.TIPODESPACHODOMICILIO_ESTANDAR)) {
//								assertTrue("El costo de despacho Standar no es consistente ", FlujoCompraServices.convert(costoDespacho).equals(Singleton.getCostoDespacho()));
//
//							} else if (Singleton.getTipoDespacho().equals(Constants.TIPODESPACHODOMICILIO_PROGRAMADO)) {
//								assertTrue("El costo de despacho Standar no es consistente ", FlujoCompraServices.convert(costoDespacho).equals(Singleton.getCostoDespacho()));
//								System.out.println("Costo de Despacho Program: " + Singleton.getCostoDespacho());
//							}
//
//						} else if (Singleton.getTipoEntrega().equals("retiroEnTienda")) {
//
//							// valido con el String Free , ya que retiro en Tienda no tiene costo
//							assertTrue("no se despliega el Total del costo de Despacho FREE para retiro en Tienda",	costoDespacho.equals("Free"));
//						}
//
//					} else {
//						assertTrue("No se despliega el monto Total del Costo de Despacho de la orden en el metodo de pago",false);
//					}
//
//				} else {
//					assertTrue("No se despliega el componente Tipo de Despacho en el Método de Pago", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege del Costo de Envio  : " + e);
//		}
//	}
//
//	@Then("^valido metodo de pago$")
//	public void valido_metodo_de_pago() throws Throwable {
//		try {
//			if (element != null) {
//
//				if (GenericMethods.existElement(By.xpath("//div[@class='payment-info']"))) {
//					assertEquals("No se despliega el texto Método de pago", "Método de pago:", BaseFlow.driver.findElement(By.xpath("//div[@class='payment-method-info']/span")).getText());
//					if (GenericMethods.existElement(By.xpath("//div[@class='list-summary']/dl/div[@class='payment-info']"))) {
//						String medioPago = BaseFlow.driver.findElement(By.xpath("//div[@class='list-summary']/dl/div[@class='payment-info']/div/div[@class='card-info']/div[1]")).getText();
//						String tipoTarjeta = BaseFlow.driver.findElement(By.xpath("//div[@class='list-summary']/dl/div[@class='payment-info']/div/div[@class='card-info']/div/img")).getAttribute("title");
//						System.out.println("Tipo de tarjeta: "+tipoTarjeta);
//
//						if (medioPago != null) {
//							// FlujoCompraServices.convert(sub_total);
//							// TODO compara con medio de pago
//							assertTrue("El Metodo de Pago no es consistente" + medioPago + "-" + Singleton.getMetodoPago(),	medioPago.equals(Singleton.getMetodoPago()));
//
//						} else {
//							assertTrue("no se despliega el Tipo del Método de Pago", false);
//						}
//
//					} else {
//						assertTrue("No se despliega el tipo de Método de pago", false);
//					}
//
//				} else {
//					assertTrue("No se despliega el componente Método de Pago", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege del Total del Método de Pago  : " + e);
//		}
//	}
//
//	@Then("^valido despliegue y formato de numero de tarjeta$")
//	public void valido_despliegue_y_formato_de_numero_de_tarjeta() throws Throwable {
//		try {
//			if (element != null) {
//
//				if (GenericMethods.existElement(By.xpath("//div[@class='cc-number']"))) {
//					assertTrue("No se despliega número de tarjeta",!BaseFlow.driver.findElement(By.xpath("//div[@class='cc-number']")).getText().equals(""));
//
//					// extraemos los ultimo 4 digitos de la tarjeta
//					System.out.println(BaseFlow.driver.findElement(By.xpath("//div[@class='cc-number']")).getText());
//					String digTar = Singleton.getnTarjeta().replaceAll("\\d{10,12}", "**** **** **** ");
//					// System.out.println(digTar);
//					assertTrue("Número de Tarjeta no cumple con el formato", BaseFlow.driver.findElement(By.xpath("//div[@class='cc-number']")).getText().equals(digTar));
//
//				} else {
//					assertTrue("No se despliega el componenete de número de tarjeta de crédito", false);
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege del formato de la tarjeta  : " + e);
//		}
//	}
//
//	@Then("^valido el despliegue de numero de cuotas$")
//	public void valido_el_despliegue_de_numero_de_cuotas() throws Throwable {
//		try {
//			if (element != null) {
//
//				// TODO realizar validación en caso de que no sea tarjeta de credito ,
//				// !Singleton.getMetodoPago().equals("Redcompra Card)
//				if (!Singleton.getMetodoPago().equals("Redcompra")) {
//					assertTrue("No existe el componente de cuotas",	BaseFlow.driver.findElements(By.xpath("//div[@class='box-t-carro-uni installments']/dl/dt/strong")).size() > 0);
//					assertTrue("No se despliega el texto Número de cuotas",	BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni installments']/dl/dt/strong")).getText().equals("Número de cuotas:")); // TODO Número de plazos: por Número de
//																				// cuotas:
//					// System.out.println(BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni
//					// installments']/dl/dd")).getText());
//					assertTrue("La cantidad de cuotas desplegadas no es consistente con la seleccionada: "+ Singleton.getnCuotas() + " desplegada: "+ BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni installments']/dl/dd")).getText(),BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni installments']/dl/dd")).getText().equals(Singleton.getnCuotas()));
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Despliegue número de Cuotas  : " + e);
//		}
//
//	}
//
//	@Then("^valido el despliegue de puntos y reglas$")
//	public void valido_el_despliegue_de_puntos_y_reglas() throws Throwable {
//
//		try {
//			if (element != null) {
//
//				assertTrue("No se despliega el componente de puntos cencosud", BaseFlow.driver.findElements(By.xpath("//div[@class='box-t-carro-uni total-loyalty-points']")).size() > 0);
//				assertTrue("No se despliega el texto Puntos Cencosud", BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni total-loyalty-points']/dl/dt/strong")).getText().equals("Puntos Cencosud:"));
//				assertTrue("No tiene puntos la compra",!BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni total-loyalty-points']/dl/dd")).getText().equals(""));
//				Singleton.setPuntosCenco(FlujoCompraServices.convert(BaseFlow.driver.findElement(By.xpath("//div[@class='box-t-carro-uni total-loyalty-points']/dl/dd")).getText()));
//				System.out.println("Puntos Cenco Orden Confirmation: " + Singleton.getPuntosCenco());
//				// PUNTOS PARA CLIENTES CENCO 1.2% PARA OTRO MEDIOS DE PAGOS 0,8% DEL TOTAL
//				if (Singleton.getMetodoPago().equals("Cencosud Card") || Singleton.getMetodoPago().equals("Tmas")) {
//					double puntosCenco = (Singleton.getTotalPedido() * 0.012);
//					int pCenco = GenericMethods.round(puntosCenco);
//					System.out.println("Puntos Cenco Clientes: "+pCenco);
//					assertTrue("Los Puntos del Order Confirmation: " + Singleton.getPuntosCenco()+ " No son consistentes con los Puntos: " + pCenco + " para Clientes Cenco",
//							(pCenco == Singleton.getPuntosCenco()));
//				} else {
//					double puntosCenco = (Singleton.getTotalPedido() * 0.004);
//					int pCenco = GenericMethods.round(puntosCenco);
//					System.out.println("Puntos Cenco Otros Clientes: "+pCenco);
//					assertTrue("Los Puntos del Order Confirmation: " + Singleton.getPuntosCenco()+ " No son consistentes con los Puntos: " + pCenco + " para Otros Clientes",
//							(pCenco == Singleton.getPuntosCenco()));
//
//				}
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info("Flujo Nuevo Registro error en Valido Despliege de puntos  : " + e);
//		}
//	}
//
//	@Then("^valido el despliegue del boton Regresar a Comprar$")
//	public void valido_el_despliegue_del_boton_Regresar_a_Comprar() throws Throwable {
//		try {
//			if (element != null) {
//
//				if (BaseFlow.driver.findElements(By.xpath("//div[@class='buttons-inline text-center mb-10']/a")).size() > 0) {
//
//					GenericMethods.scrollElement(BaseFlow.driver, BaseFlow.driver.findElement(By.xpath("//div[@class='buttons-inline text-center mb-10']/a")));
//
//					assertTrue("No se despliega el nombre del boton Regresar a Comprar",BaseFlow.driver.findElement(By.xpath("//div[@class='buttons-inline text-center mb-10']/a")).getText().equals("REGRESAR A COMPRAR"));
//					assertTrue("El href del boton Regresar a Comprar no corresponde",BaseFlow.driver.findElement(By.xpath("//div[@class='buttons-inline text-center mb-10']/a")).getAttribute("href").equals("https://uat.genesisparis.cl/home"));
//					assertTrue("El vinculo del boton Regresar a Comprar no responde",ComponentUtil.sendGet1(BaseFlow.driver.findElement(By.xpath("//div[@class='buttons-inline text-center mb-10']/a"))));
//				} else {
//					assertTrue("No se despliega el boton Regresar a Comprar", false);
//				}
//
//			} else {
//				assertFalse("Error No se ha generado la OC ", true);
//			}
//		} catch (Exception e) {
//			log.info(" error validacion de componente boton regresar a comprar : " + e);
//		}
//
//	}
//
//}
}
