package cl.eom.publicsite.automation.poc.business.flow;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import cl.eom.publicsite.automation.constants.Constants;
import cl.eom.publicsite.automation.poc.model.CompraSingleton;
import cl.eom.publicsite.automation.poc.model.PageHome;
import cl.eom.publicsite.automation.poc.util.PSCException;
import cucumber.api.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;




@RunWith(Suite.class)
@SuiteClasses({})
public class BaseFlow{
	private static final Log log = LogFactory.getLog(BaseFlow.class);
	public static WebDriver driver;
	public static SessionId session;
	public static PageHome pageHome;
	//singleton de manejo de datos 
	public static CompraSingleton SingletonCompra;

	
	
	//Ejecucipn de Soucelab
	static DesiredCapabilities caps = null;
	public static final String USERNAME = "hugoHaka";
	public static final String ACCESS_KEY = "cf258cd2-347e-4954-afea-0f3864b4ec4b";
	public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";
	public static Scenario scenario=null;
	
	/**
	 * Inicializa el driver a ejecutar la prueba
	 * Inicializa los PageModel como factory
	 * @throws Exception
	 */	
	@BeforeClass
	public static void InitializeWebDriver() throws Exception {
		
		//COMENTAR setDriverDesa() para ejecución en soucelab
		setDriverDesa();
//		capabilityBrowser(System.getenv("navegador")); // config jenkin
		//Ejecución en Soucelab
//		capabilityBrowser(Constants.Browser); // configs local
//		driver = new RemoteWebDriver(new java.net.URL(URL), caps);
	
		
	    pageHome = PageFactory.initElements(driver, PageHome.class);
	    //singleton
	    SingletonCompra = PageFactory.initElements(driver, CompraSingleton.class);
	  
	}

	@AfterClass
	public static void setUpFinal() throws Exception {
//		BaseFlow.driver.quit();
//		GenericMethods.addToZipFile(scenario);
	}	
	/**
	 * Metodo que configura el driver a utilizar de forma local,
	 * el cual esta designado por la variable Browser de la clase Constant
	 * @author William_Navarrete
	 * @throws Exception
	 */
	public static void setDriverDesa() throws Exception {
		switch (Constants.Browser) {
		case "chrome":
			caps = DesiredCapabilities.chrome();
			caps.setJavascriptEnabled(true);
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/WebDriver/Chrome/Windows/chromedriver.exe" );
			driver = new ChromeDriver(caps);
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			
			break;
		case "iExplorer":
			System.setProperty("webdriver.ie.driver", Constants.iExplorerDriver);
			caps = DesiredCapabilities.internetExplorer();    
			caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);    
			caps.setCapability("requireWindowFocus", true);
			caps.setCapability("ignoreZoomSetting", true);
			driver = new InternetExplorerDriver(caps);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\WebDriver\\IExplorer\\Windows\\aut.exe");
			break;
		case "firefox":
			WebDriverManager.firefoxdriver().setup();
			ProfilesIni profile = new ProfilesIni();
			FirefoxProfile myprofile = profile.getProfile("default");
			DesiredCapabilities dc = DesiredCapabilities.firefox();
			dc.setCapability(FirefoxDriver.PROFILE, myprofile);
			dc.setCapability("marionette", true);
//			dc.setCapability("networkConnectionEnabled", true);
//	        dc.setCapability("browserConnectionEnabled", true);
			System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"\\WebDriver\\Firefox\\Windows\\64\\geckodriver.exe");
			driver = new FirefoxDriver(dc);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
			driver.manage().window().maximize();
		case "chromeMobile":
//			Map<String, Object> deviceMetrics = new HashMap<>();
//			deviceMetrics.put("width", 360);
//			deviceMetrics.put("height", 640);
//			deviceMetrics.put("pixelRatio", 3.0);
//			Map<String, Object> mobileEmulation = new HashMap<>();
//			mobileEmulation.put("deviceMetrics", deviceMetrics);
//			mobileEmulation.put("userAgent", "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");
			Map<String, String> mobileEmulation = new HashMap<>();
			mobileEmulation.put("deviceName", "iPhone 6");
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/WebDriver/Chrome/Windows/chromedriver.exe" );
			driver = new ChromeDriver(chromeOptions);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			break;
		}
	}
	
	/**
	 * Metodo que configura el driver a utilizar en SauceLabs
	 * @param dispositivo, parametro que define en que navegador ejecutara la prueba
	 * @return  una Capability de acuerdo a la configuracion del driver
	 */
	public static Capabilities capabilityBrowser(String dispositivo) {
		switch (dispositivo) {
		case "firefox":
			caps = DesiredCapabilities.firefox();
			caps.setCapability("platform", "Windows 10");
			caps.setCapability("version", "61.0");
			break;
		case "chrome":
			caps = DesiredCapabilities.chrome();
			caps.setCapability("platform", "Windows 10");
			caps.setCapability("version", "67.0");
			break;
		case "iExplorer":
		    caps = DesiredCapabilities.internetExplorer();
			caps.setCapability("platform", "Windows 10");
			caps.setCapability("version", "11.103");
		break;
		case "androidChromeMobile":
			caps = DesiredCapabilities.android();
			caps.setCapability("appiumVersion", "1.8.1");
			caps.setCapability("deviceName","Samsung Galaxy S9 WQHD GoogleAPI Emulator");
			caps.setCapability("deviceOrientation", "portrait");
			caps.setCapability("browserName", "Chrome");
			caps.setCapability("platformVersion", "7.1");
			caps.setCapability("platformName","Android");
			break;
		case "iosSafariMobile":
			caps = DesiredCapabilities.iphone();
			caps.setCapability("appiumVersion", "1.8.1");
			caps.setCapability("deviceName","iPhone X Simulator");
			caps.setCapability("deviceOrientation", "portrait");
			caps.setCapability("platformVersion","11.3");
			caps.setCapability("platformName", "iOS");
			caps.setCapability("browserName", "Safari");
		break;
		case "macSafari":
			caps = DesiredCapabilities.safari();
			caps.setCapability("platform", "macOS 10.12");
		break;
		}
		return caps;
	}
	
	/**
	 * se obtiene estado de la session del driver 
	 * @return true si session esta activa o false si session no esta activa
	 * @throws Exception
	 */
	public static Boolean isActiveSessionDriver() throws Exception {
		
		session =  ((RemoteWebDriver) driver).getSessionId();
		try {
			if(session==null) {
				throw new PSCException("Session de driver web finalizada");
			}else {
				return true;
			}
		} catch (PSCException e) {
			throw e;
		}catch (Exception e) {
			log.error(e,e);
			throw e;
		}
	}
	
	  public static boolean isjQueryLoaded() {
	        System.out.println("Waiting for ready state complete");
	        return (new WebDriverWait(BaseFlow.driver, 30)).until(new ExpectedCondition<Boolean>() {
	            public Boolean apply(WebDriver d) {
	                JavascriptExecutor js = (JavascriptExecutor) d;
	                String readyState = js.executeScript("return document.readyState").toString();
	                System.out.println("Ready State: " + readyState);
	                return (Boolean) js.executeScript("return !!window.jQuery && window.jQuery.active == 0");
	            }
	        });
	    }
}
