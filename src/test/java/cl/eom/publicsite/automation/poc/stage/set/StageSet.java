package cl.eom.publicsite.automation.poc.stage.set;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import com.cucumber.listener.Reporter;

import cl.eom.publicsite.automation.dataProvider.ConfigFileReader;
//import cl.cencosud.publicsite.automation.dataProvider.ConfigFileReader;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * Created by Francisco Molina on 15/11/18.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features"
        ,glue = "cl/eom/publicsite/automation/poc/definition"
        ,monochrome = true
        ,strict = true
)
public class StageSet {
	/**
	 * configuracion afterclass establecida al terminar todo los escenarios,
	 * la cual cierra el driver y carga el reporte de la ejecucion
	 * @throws Exception
	 */
	@AfterClass
	public static void setUpFinal() throws Exception {
		Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
		Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
		Reporter.setSystemInfo("64 bit", "Windows 10");
		Reporter.setSystemInfo("Selenium", "3.7.0");
	    Reporter.setSystemInfo("Maven", "3.5.2");
	    Reporter.setSystemInfo("Java Version", "1.8.0_151");
	    Reporter.assignAuthor("HAKALAB - William Navarrete Lizana");
	    Reporter.loadXMLConfig(ConfigFileReader.getInstance().getReportConfigPath());
	   
	    Reporter.setTestRunnerOutput("Cucumber Junit Test Runner");
		Reporter.addStepLog("Step Log message goes here");
		Reporter.addScenarioLog("Scenario Log message goes here");
	}
}

