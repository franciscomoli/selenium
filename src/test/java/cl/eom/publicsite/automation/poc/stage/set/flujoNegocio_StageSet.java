package cl.eom.publicsite.automation.poc.stage.set;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

//SE INDICAN LOS TAG @ ASOCIADOS A LOS ESCENARIO DEFINIDOS EN EL .feature
@CucumberOptions(tags = {"@GoogleSearch" }

		, plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/FlujoCompraCompleto_NuevoRegistro.html" })

public class flujoNegocio_StageSet extends StageSet {

}
