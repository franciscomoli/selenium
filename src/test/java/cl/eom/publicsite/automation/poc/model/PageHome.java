package cl.eom.publicsite.automation.poc.model;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PageHome {

	//Objetos de PageModel , en esta capa se deben crear los objetos 
	// usando como criterio como primer atributo un id , clase o xpath si no existe id ni clase
	@FindBy(how = How.ID, using = "lst-ib") 
	private WebElement homeTxtBusqueda;
	@FindBy(how = How.NAME, using = "btnK")
	private WebElement homeBtnBuscarConGoogle;
	public WebElement getHomeTxtBusqueda() {
		return homeTxtBusqueda;
	}
	public void setHomeTxtBusqueda(WebElement homeTxtBusqueda) {
		this.homeTxtBusqueda = homeTxtBusqueda;
	}
	public WebElement getHomeBtnBuscarConGoogle() {
		return homeBtnBuscarConGoogle;
	}
	public void setHomeBtnBuscarConGoogle(WebElement homeBtnBuscarConGoogle) {
		this.homeBtnBuscarConGoogle = homeBtnBuscarConGoogle;
	}
	
	
}
