package cl.eom.publicsite.automation.poc.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SingletonOrder {
	
	//INSTANCIAMOS OBJETO Producto y lo declaramos como una lista
	//acceso // tipo de dato // nomrbe de la variable del tipo de dato // lo declaramos como un arreglo 
	private List<ProductoOrder> productos ;
	public ProductoOrder producto;
	public String nombre; //nombre del cliente
	public String telefono;
	public String email;
	public String direccion;
	public String tipoEntrega; // domicilio - retiroEnTienda - retiroEnTiendaTercero
	public String tipoDespacho;
	public String nTarjeta;
	public String nCuotas;
	public String metodoPago;
	public String nombreTercero;
	public String rutTercero;
	public int puntosCenco;
	public String numeroOrden;
	public int total;
	public int subTotal;
	public int totalPedido; // es el mismo valor de total que se despliega en la seccion medio de pago
    public int costoDespacho;
    public int descuentoPorCdgo;
    public int descuentoPorGiftCard;
    public String fechaDespachoRetiro;
    
    
    
    //SINGLETON PARA USO DE PARAMETROS DEFINITION MULTIPRODUCTOS 
 
	public String tipoUsuario;
    public String tipoRegistro;
    public String nombreUsuario;
    public String pass;
	
	
    


//	public String[] nombre_arreglo;
	
	
	
	
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public int getDescuentoPorCdgo() {
		return descuentoPorCdgo;
	}

	public int getDescuentoPorGiftCard() {
		return descuentoPorGiftCard;
	}

	public void setDescuentoPorGiftCard(int descuentoPorGiftCard) {
		this.descuentoPorGiftCard = descuentoPorGiftCard;
	}

	public void setDescuentoPorCdgo(int descuentoPorCdgo) {
		this.descuentoPorCdgo = descuentoPorCdgo;
	}

	
	

	public String getTipoEntrega() {
		return tipoEntrega;
	}

	public void setTipoEntrega(String tipoEntrega) {
		this.tipoEntrega = tipoEntrega;
	}

	public String getNumeroOrden() {
		return numeroOrden;
	}

	public void setNumeroOrden(String numeroOrden) {
		this.numeroOrden = numeroOrden;
	}

	public int getPuntosCenco() {
		return puntosCenco;
	}

	public void setPuntosCenco(int puntosCenco) {
		this.puntosCenco = puntosCenco;
	}
	public String getnCuotas() {
		return nCuotas;
	}

	public void setnCuotas(String nCuotas) {
		this.nCuotas = nCuotas;
	}

	public String getnTarjeta() {
		return nTarjeta;
	}

	public void setnTarjeta(String nTarjeta) {
		this.nTarjeta = nTarjeta;
	}
	
	
	public String getMetodoPago() {
		return metodoPago;
	}

	public void setMetodoPago(String metodoPago) {
		this.metodoPago = metodoPago;
	}

	public int getCostoDespacho() {
		return costoDespacho;
	}

	public void setCostoDespacho(int costoDespacho) {
		this.costoDespacho = costoDespacho;
	}

	public int getTotalPedido() {
		return totalPedido;
	}

	public void setTotalPedido(int totalPedido) {
		this.totalPedido = totalPedido;
	}


	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}


	public String getTipoDespacho() {
		return tipoDespacho;
	}

	public void setTipoDespacho(String tipoDespacho) {
		this.tipoDespacho = tipoDespacho;
	}

	public int getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(int subTotal) {
		this.subTotal = subTotal;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<ProductoOrder> getProductos() {
		return productos;
	}

	public void setProductos(List<ProductoOrder> productos) {
		this.productos = productos;
	}

	public ProductoOrder getProducto() {
		return producto;
	}

	public void setProducto(ProductoOrder producto) {
		this.producto = producto;
	}

	public String getNombreTercero() {
		return nombreTercero;
	}

	public void setNombreTercero(String nombreTercero) {
		this.nombreTercero = nombreTercero;
	}

	public String getRutTercero() {
		return rutTercero;
	}

	public void setRutTercero(String rutTercero) {
		this.rutTercero = rutTercero;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	   
    public String getFechaDespachoRetiro() {
		return fechaDespachoRetiro;
	}

	public void setFechaDespachoRetiro(String fechaDespachoRetiro) {
		this.fechaDespachoRetiro = fechaDespachoRetiro;
	}

}
