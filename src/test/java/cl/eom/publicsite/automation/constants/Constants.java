// SE DEFINEN LAS CONSTANTES A USAR EN EL FLUJO ,ejmp: TIPO DE MEDIOS DE PAGOS, TIPO DE PRODUCTOS (STANDAR, VARIATION, BUNDLE , ETC)


package cl.eom.publicsite.automation.constants;

public class Constants {
	public static final String Browser = "chrome";
	//"iosSafariMobile":
	//"macSafari":
	//"chromeMobile":
	
	public static final String iExplorerDriver = System.getProperty("user.dir")+ "\\WebDriver\\IExplorer\\Windows\\64\\IEDriverServer.exe";
	public static final String URL_GOOGLE = "http://www.google.cl";
	public static final String SCREENSHOT_PATH = System.getProperty("user.dir")+"/target/cucumber-reports/";

}
