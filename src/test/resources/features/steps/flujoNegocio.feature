Feature: se realiza busqueda en google

  Scenario Outline: Busqueda en Google
    Given ingreso url<url>
    When Ingreso busqueda<stringBusqueda>
    And valido busqueda<tipoBusqueda>
    Then verifico que se despliegue la busqueda

    # *** Indicadores ***
    # agregarBonificacion = true - false
    # seleccionCarroOrPago = comprar - carro
    #	guardarTarjeta = true - false
    #	ingresoCarro = true - false
    #	tipoRegistro = home -checkout
    #	tipoDocumento = RUT - DNI
    #	tipoProducto = variation - standar - option - bundle - preSale
    #	tipoDespacho = standard - program (solo aplica para despacho domicilio ,el campo se debe dejar vacion en caso de Retiro en tienda)
    #	tipoUbicacion = manual - automatica
    #	tipoEntrega = domicilio - retiroEnTienda - retiroenTiendaTercero
    # ingresaFactura = true - false
    #	metodoPago = giftCard -  cencosudCard - tmasCard - creditCard - redcompraCard
    @GoogleSearch
    Examples: 
      | url             | stringBusqueda             | tipoBusqueda |
      | "www.google.cl" | "index/tv series/mr robot" | "script"     |

  #VALIDACIONES DE TIPO DE COMPONENTE
  @ValidaDespliegedebusqueda
  Scenario: Validar Busqueda
    Then Valido despliegue de busqueda

  @ValidaBusqueda
  Scenario: Validar resultado
    Then Valido que se despliege de resultados de busqueda
