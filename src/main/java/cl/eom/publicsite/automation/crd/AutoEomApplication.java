package cl.eom.publicsite.automation.crd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoEomApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutoEomApplication.class, args);
	}
}
