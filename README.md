# README

# Arquetipo_Selenium
**Note:**
Para Poder Montar este Proyecto debemos :

Instalacion Completa http://toolsqa.com/cucumber-tutorial/

**1.-** **Instalar JDK**  https://www.oracle.com/technetwork/java/javase/downloads/index.html
    Pasos para la Instalación http://toolsqa.com/selenium-webdriver/download-and-install-java/
    
**2.-** **Instalar Eclipse** http://www.eclipse.org/downloads/	

**3.-** **Configurar variables de entorno JAVA** https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/
Install the JDK software.

Go to http://java.sun.com/javase/downloads/index.jsp.

Select the appropriate JDK software and click Download.

The JDK software is installed on your computer, for example, at C:\Program Files\Java\jdk1.6.0_02. You can move the JDK software to another location if desired.

Set JAVA_HOME:

Right click My Computer and select Properties.

On the Advanced tab, select Environment Variables, and then edit JAVA_HOME to point to where the JDK software is located, for example, C:\Program Files\Java\jdk1.6.0_02.

**4.-** **Instalar MAVEN**y configurar las varibales de entorno de windows  https://www.mkyong.com/maven/how-to-install-maven-in-windows/, adjunto en rar dentro del repositorio

**5.-**  **WebDriver** asociados a la version de Navegadores que queremos probar https://www.seleniumhq.org/download/ ,http://chromedriver.chromium.org/downloads

**6.-** **Instalar Cucumber** desde Eclipse Install New Software http://toolsqa.com/cucumber/install-cucumber-eclipse-plugin/ url para eclipse :http://cucumber.github.com/cucumber-eclipse/update-site

**7.-** Luego de Clonar el Repositorio dentro del Workspace de Eclipse , debemos importarlo como Proyecto Maven y luego realizar el update Proyect para actualizar las dependencias contenidas en el pom.xlm